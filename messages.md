# Message header structure (8 bytes)

If a message contains a non null `id` it should be ACKed by sending an `Ack` with the given 
`type`.

The following table describe the header of each message.

| Type   | Size (bytes) | Description             |
| ------ | ------------ | ----------------------- |
| Uint16 | 2            | The message body length |
| Uint16 | 2            | The message type        |
| Uint32 | 4            | Possible ACK identifier |

# General communication
## `Ack` (2 bytes)

Message type is `0x2`

Whenever a message needs to be ACKed the `Ack` message needs to be send with the header `id` set 
to the `id` of the message that needs to be acknowledged and the body should contain the message 
type.

Sending an `Ack` for a message that is either unknown or not send to your client will result in a 
`FatalError` with the error `Unknown acknowledgement (<message_id>)`

| Type   | Size (bytes) | Description                                        |
| ------ | ------------ | -------------------------------------------------- |
| Uint16 | 2            | The message type for which this acknowledgement is |

## `KeepAlive` (8 bytes)

Message type is `0x1`

The server will send out a KeepAlive message each tick to all clients, each containing a timestamp. 
If the client does not acknowledge them for over <time> seconds, the server will kick the client.

**Note**: Should be ACKed.

| Type   | Size (bytes) | Description               |
| ------ | ------------ | ------------------------- |
| Uint64 | 8            | The timestamp of the ping |

## `FatalError` (4 bytes)

Message type is `0x3`

Whenever a fatal error occurs this message will be send and the connected client will be 
disconnected.

| Type   | Size (bytes) | Description     |
| ------ | ------------ | --------------- |
| String | 0            | The error given |

# Handshake stage
## Clientbound

No clientbound message in the Handshake state.

## Serverbound
## `Handshake` (2 bytes)

Message type is `0x0`

Send by the client to initiate a connection request.

**Note**: Should be ACKed.

| Type   | Size (bytes) | Description |
| ------ | ------------ | ----------- |
| Uint16 | 2            |             |

# Login stage

The login process is as followed:
- Client -> Server: Handshake
- Server -> Client: Acknowledgement of the handshake
- Client -> Server: Login request

## Clientbound
## `LoginSuccess` (6 bytes)

Message type is `0x5`

| Type   | Size (bytes) | Description                  |
| ------ | ------------ | ---------------------------- |
| Uint16 | 2            | The given slot in the server |
| String | 0            | The player's username        |

## Serverbound
## `LoginRequest` (4 bytes)

Message type is `0x4`

| Type   | Size (bytes) | Description           |
| ------ | ------------ | --------------------- |
| String | 0            | The player's username |

## `PlayerControl` (19 bytes)

Message type is `0x6`

Player control oWo

| Type   | Size (bytes) | Description                    |
| ------ | ------------ | ------------------------------ |
| Byte   | 1            | The slot of the player         |
| Uint16 | 2            | The current ping of the player |
| Uint16 | 2            | Position x of the Player       |
| Uint16 | 2            | Position y of the Player       |
| Int16  | 2            | Velocity x of the Player       |
| Int16  | 2            | Velocity y of the Player       |
| Uint64 | 8            |                                |

