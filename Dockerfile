# Specify the Dart SDK base image version using dart:<version> (ex: dart:2.12)
FROM dart:stable AS build

# Resolve app dependencies.
WORKDIR /app
COPY ./ironclad/ ./ironclad/
COPY ./game_messages/ ./game_messages/
COPY ./server/pubspec.* ./server/

# Switch to working directory.
WORKDIR /app/server
RUN dart pub get

# Copy app source code and AOT compile it.
COPY ./server/ .

# Ensure packages are still up-to-date if anything has changed
RUN dart pub get --offline
RUN dart compile exe bin/server.dart -o bin/server

# ----------------------------------------------------------------------------- #
# Build minimal serving image from AOT-compiled `/server` and required system   #
# libraries and configuration files stored in `/runtime/` from the build stage. #
# ----------------------------------------------------------------------------- #
FROM scratch
COPY --from=build /runtime/ /
COPY --from=build /app/server/bin/server /app/bin/

# Exposing working port.
EXPOSE 8080

# Run the compiled binary.
CMD ["/app/bin/server"]