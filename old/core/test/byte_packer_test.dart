import 'package:test/test.dart';

class BytePacker {
  static int pack(
    List<int> values, {
    required List<int> maxBitsPerValue,
  }) {
    assert(values.length == maxBitsPerValue.length);

    var bits = 0;
    var offset = 0;
    for (var i = 0; i < values.length; i++) {
      final curValue = values[i];

      if (curValue.toRadixString(2).length > maxBitsPerValue[i]) {
        throw Exception(
          'Value $curValue(${curValue.toRadixString(2).length} bits) is over the max bit length(${maxBitsPerValue[i]})',
        );
      }

      for (var j = 0; j <= maxBitsPerValue[i]; j++) {
        if ((curValue & 1 << j) != 0) {
          bits |= 1 << j + offset;
        }
      }
      offset += maxBitsPerValue[i];
    }

    return bits;
  }

  static List<int> unpack(
    int bits, {
    required List<int> maxBitsPerValue,
  }) {
    final values = <int>[];
    var offset = 0;
    for (var i = 0; i < maxBitsPerValue.length; i++) {
      var curValue = 0;
      
      final end = maxBitsPerValue[i];

      for (var j = offset; j < offset + end; j++) {
        if ((bits & 1 << j) != 0) {
          curValue |= 1 << j - offset;
        }
      }
      offset += maxBitsPerValue[i];
      values.add(curValue);
    }
    return values;
  }
}

void main() {
  final result = BytePacker.pack(
    [12, 63, 63],
    maxBitsPerValue: [20, 6, 6],
  );
  print(result.toRadixString(2));
  print('binary = $result (${result.toRadixString(2).length} bits)');
  print(BytePacker.unpack(result, maxBitsPerValue: [20, 6, 6]));

  // final maxBytesPerValue = [9, 5];

  // group('BytePacker.pack', () {
  //   test('Should pack values in correct byte value', () {
  //     final result = BytePacker.pack(
  //       [2024, 63],
  //       maxBitsPerValue: maxBytesPerValue,
  //     );

  //     expect(result, 32767);
  //     expect(
  //       result.toRadixString(2).length - 1,
  //       maxBytesPerValue.fold<int>(0, (a, b) => a + b),
  //     );
  //   });

  //   test('Should throw an assertion if  values in correct byte value', () {
  //     final result = BytePacker.pack(
  //       [1023, 63],
  //       maxBitsPerValue: maxBytesPerValue,
  //     );

  //     expect(result, 32767);
  //     expect(
  //       result.toRadixString(2).length - 1,
  //       maxBytesPerValue.fold<int>(0, (a, b) => a + b),
  //     );
  //   });

  //   // final result = buildItem([1023, 63], maxBitsPerValue: [9, 5]);
  //   // print(result.toRadixString(2).length - 1 == 9 + 5);
  //   // print(retrieveItems(result, maxBitsPerValue: [9, 5]));
  //   // final awesome = Awesome();
  // });
}
