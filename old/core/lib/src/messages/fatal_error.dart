part of core.messages;

/// client <-- server
class FatalError extends Message {
  late final String error;

  FatalError(this.error) : super(MessageType.fatalError);

  FatalError.fromPayload(Uint8List payload) : super(MessageType.fatalError) {
    final reader = PayloadReader(payload);
    error = reader.getString();
  }

  @override
  void payload(PayloadBuilder payload) {
    payload.addString(error, description: 'The error given');
  }

  @override
  String toString() {
    return 'FatalError { $error }';
  }
}
