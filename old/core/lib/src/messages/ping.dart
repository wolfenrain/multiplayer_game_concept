part of core.messages;

class KeepAlive extends Message with Acknowledge {
  late final int timestamp;

  @override
  int get timeout => 10000;

  KeepAlive()
      : timestamp = DateTime.now().millisecondsSinceEpoch,
        super(MessageType.keepAlive);

  KeepAlive.fromPayload(Uint8List payload) : super(MessageType.keepAlive) {
    final reader = PayloadReader(payload);
    timestamp = reader.getUint64();
  }

  @override
  void payload(PayloadBuilder payload) {
    // TODO: find other solution, maybe only store in memory on server.
    payload.addUint64(
      timestamp,
      description: 'The timestamp of the ping',
    );
  }
}
