part of core.messages;

class Ack extends Message {
  late final MessageType ackForType;

  Ack(this.ackForType) : super(MessageType.ack);

  Ack.fromPayload(Uint8List payload) : super(MessageType.ack) {
    final reader = PayloadReader(payload);
    ackForType = MessageType(reader.getUint16());
  }

  @override
  void payload(PayloadBuilder payload) {
    payload.addUint16(
      ackForType.value,
      description: 'The message type for which this acknowledgement is',
    );
  }

  @override
  String toString() {
    return 'Ack { id: $id, ackForType: $ackForType }';
  }
}

mixin Acknowledge on Message {
  /// Timeout in milliseconds for the ACK to be expected.
  int get timeout;
}
