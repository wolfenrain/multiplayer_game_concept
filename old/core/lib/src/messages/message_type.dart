part of core.messages;

class MessageType {
  static const handshake = MessageType(0x00);
  static const keepAlive = MessageType(0x01);
  static const ack = MessageType(0x02);
  static const fatalError = MessageType(0x03);
  static const loginRequest = MessageType(0x04);
  static const loginSuccess = MessageType(0x05);
  static const playerControl = MessageType(0x06);

  final int value;

  const MessageType(this.value);

  @override
  String toString() => '${value.toRadixString(16)}';

  @override
  bool operator ==(Object object) {
    if (object is MessageType) {
      return value == object.value;
    }
    return false;
  }
}
