part of core.messages;

class ControlFlags {
  final bool up;

  final bool down;

  final bool left;

  final bool right;

  ControlFlags({
    this.up = false,
    this.down = false,
    this.left = false,
    this.right = false,
  });
}

// TODO: order of messages

/// client <-> server
///
/// # client --> server
///
/// The client sends this whenever a control is used. The server enforces the
/// playerSlot, forcing it to another slot will just move yourself
///
/// # client <-- server
///
/// The server sends this to notify the client of other players movements.
class PlayerControl extends Message {
  /// Player slot is ignored when server receives this message.
  late final int playerSlot;

  late final int ping;

  late final ControlFlags controlFlags;

  late final double playerX;

  late final double playerY;

  late final double velocityX;

  late final double velocityY;

  late final int timestamp;

  PlayerControl({
    required this.playerSlot,
    required this.ping,
    required this.controlFlags,
    required this.playerX,
    required this.playerY,
    required this.velocityX,
    required this.velocityY,
  }) : super(MessageType.playerControl) {
    timestamp = DateTime.now().millisecondsSinceEpoch;
  }

  PlayerControl.fromPayload(Uint8List payload)
      : super(MessageType.playerControl) {
    final reader = PayloadReader(payload);
    playerSlot = reader.getByte();
    ping = reader.getUint16();

    // final flags = reader.getFlags();
    // controlFlags = ControlFlags(
    //   up: flags[0],
    //   down: flags[1],
    //   left: flags[2],
    //   right: flags[3],
    // );

    playerX = reader.getUint16().toDouble();
    playerY = reader.getUint16().toDouble();

    velocityX = reader.getInt16().toDouble();
    velocityY = reader.getInt16().toDouble();

    timestamp = reader.getUint64();
  }

  @override
  void payload(PayloadBuilder payload) {
    payload.addByte(playerSlot, description: 'The slot of the player');
    payload.addUint16(ping, description: 'The current ping of the player');

    // payload.addFlag([
    //   controlFlags.up,
    //   controlFlags.down,
    //   controlFlags.left,
    //   controlFlags.right,
    // ]);

    payload.addUint16(playerX.toInt(), description: 'Position x of the Player');
    payload.addUint16(playerY.toInt(), description: 'Position y of the Player');

    payload.addInt16(
      velocityX.toInt(),
      description: 'Velocity x of the Player',
    );
    payload.addInt16(
      velocityY.toInt(),
      description: 'Velocity y of the Player',
    );

    payload.addUint64(timestamp);
  }
}
