part of core.messages;

class LoginSuccess extends Message {
  late final int slot;

  late final String username;

  LoginSuccess(this.slot, this.username) : super(MessageType.loginSuccess);

  LoginSuccess.fromPayload(Uint8List payload)
      : super(MessageType.loginSuccess) {
    final reader = PayloadReader(payload);
    slot = reader.getUint16();
    username = reader.getString();
  }

  @override
  void payload(PayloadBuilder payload) {
    payload.addUint16(slot, description: 'The given slot in the server');
    payload.addString(username, description: 'The player\'s username');
  }
}
