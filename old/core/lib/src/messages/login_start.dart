part of core.messages;

class LoginRequest extends Message {
  late final String username;

  LoginRequest(this.username) : super(MessageType.loginRequest);

  LoginRequest.fromPayload(Uint8List payload)
      : super(MessageType.loginRequest) {
    final reader = PayloadReader(payload);
    username = reader.getString();
  }

  @override
  void payload(PayloadBuilder payload) {
    payload.addString(username, description: 'The player\'s username');
  }
}
