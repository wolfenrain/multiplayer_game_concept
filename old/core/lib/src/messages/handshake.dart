part of core.messages;

class Handshake extends Message with Acknowledge {
  late final int version;

  Handshake()
      : version = 1,
        super(MessageType.handshake);

  Handshake.fromPayload(Uint8List payload) : super(MessageType.handshake) {
    final reader = PayloadReader(payload);
    version = reader.getUint16();
  }

  @override
  void payload(PayloadBuilder payload) {
    payload.addUint16(version);
  }

  @override
  int get timeout => 30000;
}
