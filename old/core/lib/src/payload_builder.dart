import 'dart:collection';
import 'dart:convert';
import 'dart:typed_data';

enum TypeFormat {
  Byte,
  Int8,
  Uint16,
  Int16,
  Uint32,
  Int32,
  Uint64,
  Double,
  String,
  Flags,
}

class ByteEntry<T> {
  final TypeFormat type;

  final T data;

  final String description;

  String get typeName => type.toString().split('.').last;

  int get offset => getOffset(type);

  ByteEntry(this.type, this.data, String? description)
      : description = description ?? '';

  static int getOffset(TypeFormat type) {
    switch (type) {
      case TypeFormat.Flags:
      case TypeFormat.Byte:
      case TypeFormat.Int8:
        return 1;
      case TypeFormat.Uint16:
      case TypeFormat.Int16:
        return 2;
      case TypeFormat.Double:
      case TypeFormat.Uint32:
      case TypeFormat.Int32:
        return 4;
      case TypeFormat.Uint64:
        return 8;
      case TypeFormat.String:
        return 0;
    }
  }
}

class PayloadReader {
  final Uint8List _payload;

  late final ByteData _byteData;

  var _offset = 0;

  PayloadReader(this._payload) {
    _byteData = ByteData.view(_payload.buffer);
  }

  List<bool> getFlags() {
    var flagValue = _byteData.getUint8(_offset);
    final flags = <bool>[];
    for (var i = 0; i < 8; i++) {
      if ((flagValue & 1 << i) != 0) {
        flags.add(true);
      } else {
        flags.add(false);
      }
    }
    _offset += ByteEntry.getOffset(TypeFormat.Flags);

    return flags.reversed.toList();
  }

  int getByte() => _get(TypeFormat.Byte) as int;
  int getInt8() => _get(TypeFormat.Int8) as int;

  int getUint16() => _get(TypeFormat.Uint16) as int;
  int getInt16() => _get(TypeFormat.Int16) as int;

  int getUint32() => _get(TypeFormat.Uint32) as int;
  int getInt32() => _get(TypeFormat.Int32) as int;

  int getUint64() => _get(TypeFormat.Uint64) as int;

  double getDouble() => _get(TypeFormat.Double) as double;

  String getString() {
    final length = _byteData.getInt32(_offset);
    _offset += 4;

    final val = _payload.skip(_offset).take(length).toList();
    _offset += val.length;

    return utf8.decode(val);
  }

  Uint8List getRest() {
    return Uint8List.fromList(
      _payload.skip(_offset).take(_payload.lengthInBytes - _offset).toList(),
    );
  }

  num _get(TypeFormat type) {
    num val;
    switch (type) {
      case TypeFormat.Byte:
        val = _byteData.getUint8(_offset);
        break;
      case TypeFormat.Int8:
        val = _byteData.getInt8(_offset);
        break;
      case TypeFormat.Uint16:
        val = _byteData.getUint16(_offset);
        break;
      case TypeFormat.Int16:
        val = _byteData.getInt16(_offset);
        break;
      case TypeFormat.Uint32:
        val = _byteData.getUint32(_offset);
        break;
      case TypeFormat.Int32:
        val = _byteData.getInt32(_offset);
        break;
      case TypeFormat.Uint64:
        val = _byteData.getUint64(_offset);
        break;
      case TypeFormat.Double:
        val = _byteData.getFloat32(_offset);
        break;
      case TypeFormat.Flags:
      case TypeFormat.String:
        throw Exception('Should not get here');
    }
    _offset += ByteEntry.getOffset(type);
    return val;
  }
}

class PayloadBuilder {
  final List<ByteEntry> _entries = [];

  List<ByteEntry> get entries => UnmodifiableListView(_entries);

  int get length => _entries.fold<int>(0, (previousValue, entry) {
        if (entry.type == TypeFormat.String) {
          final encoded = utf8.encode(entry.data);
          return previousValue + encoded.length + 4;
        }
        return previousValue + entry.offset;
      });

  void addFlag(List<bool> val, {String? description}) {
    assert(val.length <= 8);
    _entries.add(ByteEntry(TypeFormat.Flags, val, description));
  }

  void addByte(int val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Byte, val, description));
  void addInt8(int val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Int8, val, description));

  void addUint16(int val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Uint16, val, description));
  void addInt16(int val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Int16, val, description));

  void addUint32(int val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Uint32, val, description));
  void addInt32(int val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Int32, val, description));

  void addUint64(int val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Uint64, val, description));

  void addDouble(double val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.Double, val, description));

  void addString(String val, {String? description}) =>
      _entries.add(ByteEntry(TypeFormat.String, val, description));

  String describe() {
    return '''
Payload ($length bytes) {
  ${_entries.map((entry) => '${entry.typeName}: ${entry.data} (${entry.offset} bytes)').join('\n  ')}
}
''';
  }

  Uint8List build() {
    final payload = Uint8List(length);
    final byteData = ByteData.view(payload.buffer);

    var offset = 0;
    for (final entry in _entries) {
      switch (entry.type) {
        case TypeFormat.Byte:
          byteData.setUint8(offset, entry.data);
          break;
        case TypeFormat.Int8:
          byteData.setInt8(offset, entry.data);
          break;
        case TypeFormat.Uint16:
          byteData.setUint16(offset, entry.data);
          break;
        case TypeFormat.Int16:
          byteData.setInt16(offset, entry.data);
          break;
        case TypeFormat.Uint32:
          byteData.setUint32(offset, entry.data);
          break;
        case TypeFormat.Int32:
          byteData.setInt32(offset, entry.data);
          break;
        case TypeFormat.Uint64:
          byteData.setUint64(offset, entry.data);
          break;
        case TypeFormat.Double:
          byteData.setFloat32(offset, entry.data);
          break;
        case TypeFormat.String:
          final encoded = utf8.encode(entry.data);

          byteData.setInt32(offset, encoded.length);
          offset += 4;

          for (final byte in encoded) {
            byteData.setUint8(offset++, byte);
          }
          break;
        case TypeFormat.Flags:
          var val = 0;
          for (var i = 0; i < entry.data.length; i++) {
            final flag = entry.data[i];
            final bool = flag ? 1 : 0;
            val |= bool << i;
          }
          byteData.setUint8(offset, val);
          break;
      }
      offset += entry.offset;
    }

    return payload;
  }
}
