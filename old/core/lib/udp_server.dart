import 'dart:async';
import 'dart:io';

import 'package:dartlin/dartlin.dart';

import 'package:core/controller.dart';
import 'package:core/messages.dart';
import 'package:core/udp_client.dart';

class UdpServer {
  final List<Controller> _controllers = [];

  final InternetAddress address;

  final int port;

  final InternetAddress? fromAddress;
  final int? fromPort;

  final _clients = <UdpClient>[];

  RawDatagramSocket? _udpSocket;

  List<int> _sendingBytes = [];
  int _sending = 0;

  /// Receiving bandwidth in bytes
  int get sending => _sending;

  List<int> _receivingBytes = [];
  int _receiving = 0;

  /// Receiving bandwidth in bytes
  int get receiving => _receiving;

  late Timer _byteClearer;

  final bool logging;

  final Map<UdpClient, Map<MessageType, int>> _messageCounters = {};

  final Map<UdpClient, List<Message>> _needsAcknowledgment = {};

  final Map<UdpClient, Map<int, Timer>> _acknowledgmentTimeouts = {};

  UdpServer(
    this.address,
    this.port, {
    this.fromAddress,
    this.fromPort,
    this.logging = false,
  }) : assert(
          fromAddress != null && fromPort != null ||
              fromAddress == null && fromPort == null,
        ) {
    _byteClearer = Timer.periodic(Duration(seconds: 1), (_) {
      _sending = _sendingBytes.fold<int>(0, (a, b) => a + b);
      _receiving = _receivingBytes.fold<int>(0, (a, b) => a + b);
      _sendingBytes = [];
      _receivingBytes = [];
    });
  }

  void onConnected(UdpClient client) {}
  void onDisconnected(UdpClient client) {}

  void dispose() {
    _udpSocket?.close();
    _byteClearer.cancel();
    _acknowledgmentTimeouts.forEach((key, value) {
      value.values.forEach((t) => t.cancel());
    });
  }

  void registerController(Controller controller) {
    _controllers.add(controller);
  }

  Controller? getController<T extends Message>(T message) {
    return _controllers.firstOrNull(
      (c) => c.type == (message is Ack ? message.ackForType : message.type),
    );
  }

  UdpClient getClient(InternetAddress address, int port) {
    return _clients.firstWhere(
      (c) => c.address == address && c.port == port,
      orElse: () {
        final client = UdpClient(this, address: address, port: port);
        _clients.add(client);
        return client;
      },
    );
  }

  void emit(Message message, {InternetAddress? address, int? port}) {
    final realAddress = address ?? fromAddress;
    if (realAddress == null) {
      throw Exception('No address given');
    }
    final realPort = port ?? fromPort;
    if (realPort == null) {
      throw Exception('No address or port given');
    }
    final client = getClient(realAddress, realPort);

    if (message is Acknowledge) {
      _messageCounters[client] ??= {};
      _needsAcknowledgment[client] ??= [];
      _acknowledgmentTimeouts[client] ??= {};

      // Create new id for this message type.
      var counter = (_messageCounters[client]![message.type] ?? 0) + 1;
      message.id = counter;
      _messageCounters[client]![message.type] = counter;

      // Store message for acknowledgment and unacknowledgment.
      _needsAcknowledgment[client]?.add(message);
      _acknowledgmentTimeouts[client]![counter] = Timer(
        Duration(milliseconds: message.timeout),
        () {
          final controller = getController(message);
          controller?.unacknowledged(message, client);

          // Clear all stored data.
          _needsAcknowledgment[client]?.remove(message);
          _acknowledgmentTimeouts[client]?.remove(counter);
        },
      );
    }

    final payload = message.toBytes();
    _sendingBytes.add(payload.lengthInBytes);
    _udpSocket?.send(payload, realAddress, realPort);
  }

  bool allowMessage(UdpClient client, Message message) => true;

  Future<void> start() async {
    _udpSocket = await RawDatagramSocket.bind(
      address,
      port,
      reuseAddress: false,
    );

    _udpSocket?.listen((event) {
      if (event == RawSocketEvent.read) {
        final dg = _udpSocket?.receive();
        if (dg == null) return;
        _receivingBytes.add(dg.data.lengthInBytes);

        // Skip if we should only listen to specific sender.
        if (fromAddress != null &&
            dg.address != fromAddress &&
            fromPort != null &&
            dg.port == dg.port) {
          return;
        }

        // Retrieve or get the client.
        var client = getClient(dg.address, dg.port);
        if (!client.isConnected) {
          client.isConnected = true;
          onConnected(client);
        }

        Message message;
        try {
          message = Message.parse(dg.data);
        } catch (err) {
          return _log('Unable to parse message: $err');
        }

        if (!allowMessage(client, message) && message is! Ack) {
          _log('Message $message not allowed');
          return close(
            client,
            reason: 'Message with type ${message.type} is not allowed',
          );
        }

        final controller = getController(message);
        if (controller == null) {
          return _log('No controller found for "$message", ignoring');
        }

        if (message is Ack) {
          // Retrieve original message from cache.
          final foundMessage = _needsAcknowledgment[client]?.firstOrNull(
            (m) => m.id == message.id,
          );
          if (foundMessage == null) {
            _log('Received unknown acknowledgement (${message.id})');
            return client.emit(
              FatalError('Unknown acknowledgement (${message.id})'),
            );
          }

          // Clear all stored data.
          _acknowledgmentTimeouts[client]?[foundMessage.id]?.cancel();
          _needsAcknowledgment[client]?.remove(foundMessage);

          return controller.acknowledged(foundMessage, client);
        }

        controller.handle(message, client);

        // Send an Ack if this message expects it.
        if (message is Acknowledge) {
          client.emit(Ack(message.type)..id = message.id);
        }
      }
    });

    _log('Listening on port $port');
  }

  void close(UdpClient client, {String reason = 'Unknown'}) {
    if (client.isConnected) {
      _log('Closing connection "$client" with reason "$reason"');
      client.isConnected = false;
      _clients.remove(client);
      onDisconnected(client);

      // Clear all stored data.
      _acknowledgmentTimeouts[client]?.values.forEach((e) => e.cancel());
      _acknowledgmentTimeouts.remove(client);
      _needsAcknowledgment.remove(client);
      _messageCounters.remove(client);

      client.emit(FatalError(reason));
    }
  }

  void _log(Object message) {
    if (logging) {
      print('[$runtimeType] $message');
    }
  }
}
