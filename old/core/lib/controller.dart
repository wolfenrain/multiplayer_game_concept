import 'package:core/messages.dart';
import 'package:core/udp_client.dart';

abstract class Controller<T extends Message> {
  final MessageType type;

  Controller(this.type);

  /// Handle incoming message.
  void handle(T message, UdpClient client) {
    throw Exception('Unhandled message $message');
  }

  /// Handle acknowledgement of a message that was send.
  void acknowledged(T message, UdpClient client) {
    throw Exception('Unhandled acknowledgment for $message');
  }

  /// Handle unacknowledgement of a message that was send.
  void unacknowledged(T message, UdpClient client) {
    throw Exception('Unhandled unacknowledgment for $message');
  }

  @override
  String toString() {
    return '$runtimeType { type: $type, message: $T }';
  }
}
