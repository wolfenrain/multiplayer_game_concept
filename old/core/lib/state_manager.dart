class StateManager<V, T> {
  final Map<V, T> _states = {};

  final T unknownState;

  final bool logging;

  StateManager(this.unknownState, {this.logging = false});

  void set(V client, T state) {
    _log('Changing state from "${get(client)}" to "$state" for "$client"');
    _states[client] = state;
  }

  T get(V client) {
    return _states[client] ?? unknownState;
  }

  void remove(V client) {
    _log('Removing state for "$client"');
    _states.remove(client);
  }

  void _log(Object message) {
    if (logging) {
      print('[$runtimeType] $message');
    }
  }
}
