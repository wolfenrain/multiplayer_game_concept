import 'dart:async';
import 'dart:io';

import 'package:core/messages.dart';
import 'package:core/udp_server.dart';

class UdpClient {
  final UdpServer server;

  final InternetAddress address;

  final int port;

  bool isConnected;

  UdpClient(
    this.server, {
    required this.address,
    required this.port,
    this.isConnected = false,
  });

  @override
  bool operator ==(Object object) {
    if (object is UdpClient) {
      return address.address == object.address.address && port == object.port;
    }
    return false;
  }

  void emit(Message message) {
    server.emit(message, address: address, port: port);
  }

  @override
  String toString() {
    return '${address.address}:$port';
  }
}
