library core.messages;

import 'dart:convert';
import 'dart:typed_data';

import 'package:dartlin/dartlin.dart';
import 'package:core/src/payload_builder.dart';

part 'src/messages/ack.dart';
part 'src/messages/fatal_error.dart';
part 'src/messages/handshake.dart';
part 'src/messages/login_start.dart';
part 'src/messages/login_success.dart';
part 'src/messages/message_type.dart';
part 'src/messages/player_control.dart';
part 'src/messages/ping.dart';

/// Defines how a message should be formatted when it is send over the line.
///
/// | Offset | Type   | Description               |
/// | ------ | ------ | ------------------------- |
/// | 0      | Int32  | The length of the message |
/// | 4      | Uint8  | The type of the message   |
/// | 5      | UInt16 | Possible ack id           |
/// | 7      | *      | The message payload       |
///
/// The message length is the length of [type] (1) and the result of [payload]
/// in bytes. The smallest would be one without a payload (Length of 1).
abstract class Message {
  int? id;

  final MessageType type;

  Message(this.type);

  void payload(PayloadBuilder builder);

  void _header(PayloadBuilder header, int bodyLength) {
    header.addUint16(bodyLength, description: 'The message body length');
    header.addUint16(type.value, description: 'The message type');
    header.addUint32(id ?? 0, description: 'Possible ACK identifier');
  }

  Uint8List toBytes() {
    final builder = BytesBuilder();

    final body = PayloadBuilder();
    payload(body);
    if (body.length > 32767) {
      throw Exception('Body of $runtimeType is too big');
    }

    final header = PayloadBuilder();
    _header(header, body.length);

    builder.add(header.build());
    builder.add(body.build());

    return builder.toBytes();
  }

  int getLength({bool withHeaders = false}) {
    final body = PayloadBuilder();
    payload(body);

    final header = PayloadBuilder();
    _header(header, body.length);

    return (withHeaders ? header.length : 0) + body.length;
  }

  /// Generates a simple markdown table with a overview of the payload.
  String describe({bool withHeaders = false}) {
    final body = PayloadBuilder();
    payload(body);

    final header = PayloadBuilder();
    _header(header, body.length);

    var entries = <List<String>>[
      ['Type', 'Size (bytes)', 'Description'],
      if (withHeaders)
        ...header.entries.map(
          (entry) => [entry.typeName, '${entry.offset}', entry.description],
        ),
      ...body.entries.map(
        (entry) => [entry.typeName, '${entry.offset}', entry.description],
      ),
    ];

    final lengths = <int>[];

    entries.forEach((element) {
      for (var i = 0; i < element.length; i++) {
        final length = element[i].length;
        if (lengths.length <= i) {
          lengths.add(length);
        } else {
          if (lengths[i] < length) {
            lengths[i] = length;
          }
        }
      }
    });

    entries = entries
        .map(
          (e) => e.mapIndexed((i, a) => a.padRight(lengths[i], ' ')).toList(),
        )
        .toList();

    return '''
| ${entries.first.join(' | ')} |
| ${lengths.map((e) => ''.padLeft(e, '-')).join(' | ')} |
${entries.skip(1).map((e) => '| ${e.join(' | ')} |').join('\n')}
''';
  }

  @override
  String toString() {
    final builder = PayloadBuilder();
    payload(builder);
    return '$runtimeType { payload: ${builder.build()} }';
  }

  static final Map<int, Message Function(Uint8List payload)> _parsers = {};

  static Message parse(Uint8List data) {
    final reader = PayloadReader(data);
    final length = reader.getUint16();
    final type = reader.getUint16();
    final id = reader.getUint32();
    final payload = reader.getRest();

    if (payload.lengthInBytes != length) {
      throw Exception('Given payload length does not equal given length');
    }

    if (!_parsers.containsKey(type)) {
      throw Exception('No parser found for $type');
    }

    final message = _parsers[type]!(payload);
    message.id = id;
    return message;
  }

  static void add<T extends Message>(
    MessageType type,
    Message Function(Uint8List payload) parser,
  ) {
    if (_parsers.isEmpty) {
      _parsers[MessageType.ack.value] = (payload) => Ack.fromPayload(payload);
    }
    _parsers[type.value] = parser;
  }
}
