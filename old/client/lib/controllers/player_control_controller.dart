import 'package:client/components/ping_component.dart';
import 'package:client/components/previous_position_component.dart';
import 'package:client/components/slot_component.dart';
import 'package:client/components/velocity_component.dart';
import 'package:client/controllers/client_controller.dart';
import 'package:core/messages.dart';
import 'package:core/udp_client.dart';
import 'package:flame/extensions.dart';
import 'package:flame_oxygen/flame_oxygen.dart';

class PlayerControlController extends ClientController<PlayerControl> {
  PlayerControlController() : super(MessageType.playerControl);

  Future<Entity> createEntity(int slot) async {
    return game.createEntity(
      name: 'slot_${slot}',
      position: Vector2.zero(),
      size: Vector2(50, 50),
    )
      ..add<NextPositionComponent, Vector2>(Vector2.zero())
      ..add<VelocityComponent, Vector2>(Vector2.zero())
      ..add<SlotComponent, int>(slot)
      ..add<PingComponent, int>(0)
      ..add<SpriteComponent, SpriteInit>(
        SpriteInit(await game.loadSprite(
          'character.png',
          srcPosition: Vector2.zero(),
          srcSize: Vector2.all(16),
        )),
      );
  }

  int previousTimestamp = 0;

  @override
  void handle(PlayerControl message, UdpClient client) async {
    if (previousTimestamp > message.timestamp) {
      print('old message, ignore');
      return;
    }
    previousTimestamp = message.timestamp;
    final position = Vector2(message.playerX, message.playerY);
    final velocity = Vector2(message.velocityX, message.velocityY);

    Entity player;
    if (message.playerSlot == connection.slot) {
      player = game.localPlayer ??= await createEntity(message.playerSlot);
    } else {
      player = game.players[message.playerSlot] ??=
          await createEntity(message.playerSlot);
    }
    final pingComponent = player.get<PingComponent>()!;
    pingComponent.value = message.ping;

    final currentPos = player.get<PositionComponent>()!;
    final nextPos = player.get<NextPositionComponent>()!;
    currentPos.position.setFrom(nextPos.position);
    nextPos.position.setFrom(position);

    if (player != game.localPlayer) {
      final velocityComp = player.get<VelocityComponent>()!;
      velocityComp.position.setFrom(velocity);
    }
  }
}
