import 'package:client/controllers/client_controller.dart';
import 'package:core/messages.dart';
import 'package:core/udp_client.dart';

class FatalErrorController extends ClientController<FatalError> {
  FatalErrorController() : super(MessageType.fatalError);

  @override
  void handle(FatalError message, UdpClient client) {
    print('Received FatalError: "${message.error}"');

    connection.errorsCtrl.add(message);
  }
}
