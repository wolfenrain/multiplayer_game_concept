import 'package:client/controllers/client_controller.dart';
import 'package:client/game_state_manager.dart';
import 'package:core/messages.dart';
import 'package:core/udp_client.dart';

class HandshakeController extends ClientController<Handshake> {
  HandshakeController() : super(MessageType.handshake);

  @override
  void handle(Handshake message, UdpClient client) {}

  @override
  void acknowledged(Handshake message, UdpClient client) {
    connection.state.set(GameState.login);
  }

  @override
  void unacknowledged(Handshake message, UdpClient client) {
    print('unacknowledged');
  }
}
