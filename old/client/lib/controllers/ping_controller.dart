import 'package:client/controllers/client_controller.dart';
import 'package:core/messages.dart';
import 'package:core/udp_client.dart';

class KeepAliveController extends ClientController<KeepAlive> {
  KeepAliveController() : super(MessageType.keepAlive);

  @override
  void handle(KeepAlive message, UdpClient client) {}
}
