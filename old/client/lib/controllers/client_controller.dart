import 'package:client/client_connection.dart';
import 'package:client/client_game.dart';
import 'package:core/messages.dart';
import 'package:core/controller.dart';

abstract class ClientController<T extends Message> extends Controller<T> {
  ClientGame get game => connection.game;

  late final ClientConnection connection;

  ClientController(MessageType type) : super(type);
}
