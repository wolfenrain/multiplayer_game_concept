import 'package:client/controllers/client_controller.dart';
import 'package:client/game_state_manager.dart';
import 'package:core/messages.dart';
import 'package:core/udp_client.dart';

class LoginSuccessController extends ClientController<LoginSuccess> {
  LoginSuccessController() : super(MessageType.loginSuccess);

  @override
  void handle(LoginSuccess message, UdpClient client) {
    connection.state.set(GameState.play);

    print('Received slot ${message.slot}');
    connection.slot = message.slot;
  }
}
