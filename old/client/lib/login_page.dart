import 'package:client/client_connection.dart';
import 'package:core/messages.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  final ClientConnection connection;

  const LoginPage(this.connection);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController? controller;

  @override
  void initState() {
    controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextField(
          controller: controller,
        ),
        ElevatedButton(
          child: Text('Login'),
          onPressed: () {
            final username = controller?.text ?? '';
            if (username == '') {
              return;
            }
            widget.connection.emit(LoginRequest(username));
          },
        ),
      ],
    );
  }
}
