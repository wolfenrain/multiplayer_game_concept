import 'dart:async';

enum GameState {
  handshake,
  login,
  play,
}

class GameStateManager {
  GameState _state = GameState.handshake;

  final _streamCtrl = StreamController<GameState>();

  late Stream<GameState> stream;

  GameStateManager() {
    stream = _streamCtrl.stream;
  }

  GameState get() => _state;

  void clear() => _state = GameState.handshake;

  void set(GameState state) {
    _streamCtrl.add(state);
    _state = state;
  }
}
