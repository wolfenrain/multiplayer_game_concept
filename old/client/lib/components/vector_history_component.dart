import 'package:flame/extensions.dart';
import 'package:flame_oxygen/flame_oxygen.dart';

abstract class VectorHistoryComponent extends Component<List<Vector2>> {
  Vector2? next;

  Vector2? current;

  @override
  void init([List<Vector2>? data]) {
    assert(data != null);
    assert(data!.length == 2);
    current = data!.first;
    next = data.last;
  }

  @override
  void reset() {
    current = null;
    next = null;
  }
}
