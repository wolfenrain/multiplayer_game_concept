import 'dart:math';

import 'package:client/client_connection.dart';
import 'package:client/client_game.dart';
import 'package:client/components/ping_component.dart';
import 'package:client/components/previous_position_component.dart';
import 'package:client/components/slot_component.dart';
import 'package:client/components/velocity_component.dart';
import 'package:client/game_state_manager.dart';
import 'package:client/joystick.dart';
import 'package:client/login_page.dart';
import 'package:client/systems/simple_render_system.dart';
import 'package:client/systems/movement_system.dart';
import 'package:core/messages.dart';
import 'package:dartlin/dartlin.dart';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame_oxygen/flame_oxygen.dart';
import 'package:flutter/material.dart' hide Viewport, KeepAlive;

void main() async {
  Message.add(MessageType.handshake, (_) => Handshake.fromPayload(_));
  Message.add(MessageType.keepAlive, (_) => KeepAlive.fromPayload(_));
  Message.add(MessageType.fatalError, (_) => FatalError.fromPayload(_));

  Message.add(MessageType.loginSuccess, (_) => LoginSuccess.fromPayload(_));

  Message.add(MessageType.playerControl, (_) => PlayerControl.fromPayload(_));

  // final connection = await ClientConnection.setup();

  // final primary = ClientGame();
  runApp(MaterialApp(home: GameApp()));
}

class GameApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text('Play'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          },
        ),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GameState? state;

  ClientConnection? connection;

  @override
  void initState() {
    ClientConnection.setup().then((connection) {
      this.connection = connection;

      connection.errors.listen((error) {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Fatal error'),
              content: Text(error.error),
              actions: [
                TextButton(
                  onPressed: () {
                    setState(() => state = GameState.handshake);
                    Navigator.of(context).pop();
                  },
                  child: Text('Close'),
                ),
              ],
            );
          },
        );
      });

      connection.state.stream.listen((state) {
        if (state == GameState.handshake) {
          connection.emit(Handshake());
        }
        setState(() => this.state = state);
      });
      connection.emit(Handshake());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) {
          if (state == null) {
            return Center(child: CircularProgressIndicator());
          }

          return when<GameState?, Widget>(state, {
            GameState.handshake: () {
              return Center(child: CircularProgressIndicator());
            },
            GameState.login: () => LoginPage(connection!),
            GameState.play: () => GameWidget(game: ClientGame(connection!))
          }).orElse(() => Text('Unknown state ${state}'));
        },
      ),
    );
  }
}
