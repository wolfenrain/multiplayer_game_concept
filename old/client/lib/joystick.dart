import 'dart:async';

import 'package:flame/extensions.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

class Joystick {
  final double outerRadius;

  final double innerRadius;

  final Vector2 center;

  Vector2 __position;

  Vector2 get _position => __position;

  set _position(Vector2 value) {
    __position = value;
    final distance = __position.distanceTo(Vector2.zero());
    if (distance > outerRadius) {
      var originToObject = __position - Vector2.zero();
      originToObject *= outerRadius / distance;
      __position = Vector2.zero() + originToObject;
    }
    _positionStreamCtrl.add(__position.clone());
  }

  int? pointerId;

  final _positionStreamCtrl = StreamController<Vector2>();

  late Stream<Vector2> position;

  Rect get bounds => Rect.fromCircle(
        center: Offset(center.x + outerRadius, center.y + outerRadius),
        radius: outerRadius,
      );

  Rect get thumbBounds => Rect.fromCircle(
        center: Offset(
          (center.x + outerRadius) + __position.x,
          (center.y + outerRadius) + __position.y,
        ),
        radius: innerRadius,
      );

  Joystick({
    this.outerRadius = 50,
    this.innerRadius = 25,
    Vector2? center,
  })  : this.center = center ?? Vector2.zero(),
        this.__position = Vector2.zero() {
    position = _positionStreamCtrl.stream;
  }

  void dispose() => _positionStreamCtrl.close();

  Vector2? startPosition;

  void onDragStart(int pointerId, Vector2 startPosition) {
    this.startPosition = startPosition;
    this.pointerId ??= pointerId;
  }

  void onDragUpdate(int pointerId, DragUpdateInfo info) {
    if (pointerId == this.pointerId) {
      _position = info.eventPosition.game - startPosition!;
    }
  }

  void onDragEnd(int pointerId, DragEndInfo info) {
    if (pointerId == this.pointerId) {
      this.pointerId = null;
      _position = Vector2.zero();
    }
  }

  void render(Canvas canvas) {
    canvas
      ..save()
      ..translate(center.x + outerRadius, center.y + outerRadius)
      ..drawCircle(Offset.zero, outerRadius, Paint()..color = Colors.white)
      ..drawCircle(
          _position.toOffset(), innerRadius, Paint()..color = Colors.red)
      ..restore();
  }
}
