import 'dart:async';
import 'dart:io';

import 'package:client/client_game.dart';
import 'package:client/controllers/client_controller.dart';
import 'package:client/controllers/fatal_error_controller.dart';
import 'package:client/controllers/handshake_controller.dart';
import 'package:client/controllers/login_success_controller.dart';
import 'package:client/controllers/ping_controller.dart';
import 'package:client/controllers/player_control_controller.dart';
import 'package:client/game_state_manager.dart';
import 'package:core/controller.dart';
import 'package:core/messages.dart';
import 'package:core/udp_server.dart';

class ClientConnection extends UdpServer {
  int? slot;

  late final ClientGame game;

  final state = GameStateManager();

  final errorsCtrl = StreamController<FatalError>();
  late Stream<FatalError> errors;

  ClientConnection(
    InternetAddress address,
    int port,
    InternetAddress server,
  ) : super(
          address,
          port,
          fromAddress: server,
          fromPort: 8080,
          logging: true,
        ) {
    registerController(HandshakeController());
    registerController(KeepAliveController());
    registerController(FatalErrorController());

    registerController(LoginSuccessController());

    registerController(PlayerControlController());

    errors = errorsCtrl.stream;
  }

  @override
  void dispose() {
    errorsCtrl.close();
    super.dispose();
  }

  static Future<ClientConnection> setup() async {
    ClientConnection? connection;
    var port = 8081;
    final server = (await InternetAddress.lookup('jochum.pc')).first;
    while (connection == null && port < 8090) {
      try {
        print('Setting up local server on port $port');
        connection = ClientConnection(InternetAddress.anyIPv4, port, server);

        await connection.start();
        print('Local server started');
      } on SocketException catch (_) {
        connection?.dispose();
        connection = null;
        port++;
      }
    }

    if (connection == null) {
      throw Exception('Failed setting up ClientConnection');
    }
    return connection;
  }

  // @override
  // void emit(Message message, {InternetAddress? address, int? port}) {
  //   Future.delayed(Duration(milliseconds: 200)).then((value) {
  //     super.emit(message, address: address, port: port);
  //   });
  // }

  @override
  void registerController(Controller controller) {
    if (controller is ClientController) {
      controller.connection = this;
    }
    super.registerController(controller);
  }
}
