import 'dart:math';

import 'package:client/client_connection.dart';
import 'package:client/components/ping_component.dart';
import 'package:client/components/previous_position_component.dart';
import 'package:client/components/slot_component.dart';
import 'package:client/components/velocity_component.dart';
import 'package:client/game_state_manager.dart';
import 'package:client/joystick.dart';
import 'package:client/systems/simple_render_system.dart';
import 'package:client/systems/movement_system.dart';
import 'package:core/messages.dart';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flame_oxygen/flame_oxygen.dart';
import 'package:flutter/material.dart' hide Viewport, KeepAlive;

class ClientGame extends OxygenGame with MultiTouchDragDetector, FPSCounter {
  late Joystick joystick;

  final ClientConnection connection;

  final Map<int, Entity> players = {};

  Entity? localPlayer;

  Vector2? get localPosition => localPlayer?.get<PositionComponent>()?.position;
  int? get ping => localPlayer?.get<PingComponent>()?.value ?? 0;

  Camera? camera;

  final debugText = TextPaint(config: TextPaintConfig(color: Colors.green));

  ClientGame(this.connection) : super() {
    connection.game = this;

    world.registerComponent(() => VelocityComponent());
    world.registerComponent(() => SlotComponent());
    world.registerComponent(() => PingComponent());
    world.registerComponent(() => NextPositionComponent());

    world.registerSystem(MovementSystem());
    world.registerSystem(SimpleRenderSystem());
  }

  @override
  void onResize(Vector2 size) {
    joystick = Joystick(
      outerRadius: 50,
      innerRadius: 25,
      center: Vector2(50, size.y - 150),
    )..position.listen((position) {
        final velocity = position * 5;
        localPlayer?.get<VelocityComponent>()?.position.setFrom(velocity);

        connection.emit(PlayerControl(
          playerSlot: 0,
          ping: 0,
          controlFlags: ControlFlags(),
          playerX: 0,
          playerY: 0,
          velocityX: velocity.x,
          velocityY: velocity.y,
        ));
      });
    super.onResize(size);
  }

  @override
  Future<void> init() async {
    await Flame.device.setLandscape();
    await Flame.device.fullScreen();

    camera = Camera();
    camera?.defaultShakeDuration = 0;
    camera?.gameRef = this;

    // await connect(); TODO: ??
  }

  void onPingTimeout() async {
    print('Local Ping timeout');
    // connection?.dispose();
    // connection = null; // TODO: disconnect?
    // await connect(); TODO: state change.
  }

  @override
  void render(Canvas canvas) {
    canvas.save();
    camera?.apply(canvas);
    camera?.followVector2(
      localPosition ?? Vector2.zero(),
      worldBounds: Vector2.zero() & Vector2(10000, 10000),
    );
    super.render(canvas);

    canvas.restore();
    joystick.render(canvas);
    final text = [
      'FPS: ${fps().toStringAsFixed(2)}',
      //   'Ping: ${ping}',
      //   'Receiving: ${humanFileSize(connection?.receiving.toDouble() ?? 0)}/s',
      //   'Sending: ${humanFileSize(connection?.sending.toDouble() ?? 0)}/s',
    ].join('\n');
    final textSize = debugText.measureText(text);

    canvas.drawRect(
      Vector2.zero() & textSize,
      Paint()..color = Colors.black.withOpacity(0.5),
    );

    debugText.render(canvas, text, Vector2.zero());
  }

  @override
  void update(double delta) {
    camera?.update(delta);
    super.update(delta);
  }

  @override
  void onDragStart(int pointerId, DragStartInfo info) {
    final startPosition = info.eventPosition.game;
    if (joystick.thumbBounds.contains(startPosition.toOffset())) {
      joystick.onDragStart(pointerId, startPosition);
    }
  }

  @override
  void onDragUpdate(int pointerId, DragUpdateInfo info) {
    joystick.onDragUpdate(pointerId, info);
    super.onDragUpdate(pointerId, info);
  }

  @override
  void onDragEnd(int pointerId, DragEndInfo info) {
    joystick.onDragEnd(pointerId, info);
  }

  String humanFileSize(double bytes, {bool si = false, int dp = 1}) {
    final thresh = si ? 1000 : 1024;

    if (bytes.abs() < thresh) {
      return '$bytes B';
    }

    final units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    var u = -1;
    final r = pow(10, dp);

    do {
      bytes /= thresh;
      ++u;
    } while (bytes.abs().round() * r / r >= thresh && u < units.length - 1);

    return '${bytes.toStringAsFixed(dp)} ${units[u]}';
  }
}
