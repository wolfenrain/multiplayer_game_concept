import 'package:client/client_game.dart';
import 'package:client/components/previous_position_component.dart';
import 'package:client/components/velocity_component.dart';
import 'package:flame_oxygen/flame_oxygen.dart';
import 'package:flame/extensions.dart';
import 'package:oxygen/oxygen.dart';

class MovementSystem extends System with UpdateSystem, GameRef<ClientGame> {
  Query? _query;

  @override
  void init() {
    _query = createQuery([
      Has<PositionComponent>(),
      Has<NextPositionComponent>(),
      Has<VelocityComponent>(),
    ]);
  }

  @override
  void update(double delta) {
    for (final entity in _query?.entities ?? <Entity>[]) {
      final position = entity.get<PositionComponent>()!;
      final next = entity.get<NextPositionComponent>()!;
      final velocity = entity.get<VelocityComponent>()!;

      position.x = (position.x + velocity.x * delta).clamp(0, 10000);
      position.y = (position.y + velocity.y * delta).clamp(0, 10000);

      position.position.lerp(next.position, delta);

      if (entity == game?.localPlayer) {
        next.position.setFrom(position.position);
      }
    }
  }
}
