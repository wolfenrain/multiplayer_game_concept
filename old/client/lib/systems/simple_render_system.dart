import 'dart:ui';

import 'package:client/client_game.dart';
import 'package:client/components/ping_component.dart';
import 'package:client/components/slot_component.dart';
import 'package:flame/extensions.dart';
import 'package:flame/game.dart';
import 'package:flame_oxygen/flame_oxygen.dart';
import 'package:flutter/material.dart';

class SimpleRenderSystem extends BaseSystem with GameRef<ClientGame> {
  @override
  List<Filter<Component>> get filters => [
        Has<SlotComponent>(),
        Has<PingComponent>(),
      ];

  @override
  void renderEntity(Canvas canvas, Entity entity) {
    final position = entity.get<PositionComponent>()!;
    final slot = entity.get<SlotComponent>()!.value!;
    final ping = entity.get<PingComponent>()!.value!;
    final size = entity.get<SizeComponent>()!.size;
    final sprite = entity.get<SpriteComponent>()?.sprite;

    canvas.drawRect(
      Vector2.zero() & size,
      Paint()..color = game!.connection.slot == slot ? Colors.red : Colors.blue,
    );

    sprite?.render(canvas, size: size);
    TextPaint(
      config: TextPaintConfig(
        color: Colors.green,
        fontSize: 12,
      ),
    ).render(
      canvas,
      [
        'slot: $slot',
        'ping: $ping',
        'pos: ${position.x.toStringAsFixed(2)}, ${position.y.toStringAsFixed(2)}',
      ].join('\n'),
      Vector2(size.x + 2, 0),
      anchor: Anchor.topLeft,
    );
  }
}
