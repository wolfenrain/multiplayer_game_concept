import 'package:core/udp_client.dart';
import 'package:oxygen/oxygen.dart';
import 'package:core/messages.dart';

class UdpClientComponent extends ValueComponent<UdpClient> {
  void emit(Message message) => value?.emit(message);
}
