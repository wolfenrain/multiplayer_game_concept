import 'package:core/messages.dart';
import 'package:core/udp_client.dart';

import 'server_controller.dart';

class KeepAliveController extends ServerController<KeepAlive> {
  KeepAliveController() : super(MessageType.keepAlive);

  Map<UdpClient, int> lastKeepAliveId = {};

  @override
  void acknowledged(KeepAlive message, UdpClient client) {
    lastKeepAliveId[client] = message.id!;
    server.setPing(
      client,
      DateTime.now().millisecondsSinceEpoch - message.timestamp,
    );
  }

  @override
  void unacknowledged(KeepAlive message, UdpClient client) {
    if ((lastKeepAliveId[client] ?? 0) < message.id!) {
      server.onTimeout(client);
    }
  }
}
