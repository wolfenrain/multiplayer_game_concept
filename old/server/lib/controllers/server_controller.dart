import 'package:oxygen/oxygen.dart';
import 'package:core/messages.dart';
import 'package:core/controller.dart';

import '../game_server.dart';

abstract class ServerController<T extends Message> extends Controller<T> {
  late final GameServer server;
  World get world => server.world;

  ServerController(MessageType type) : super(type);
}
