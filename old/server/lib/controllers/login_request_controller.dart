import 'package:core/messages.dart';
import 'package:core/udp_client.dart';
import 'package:server/components/position_component.dart';
import 'package:server/components/slot_component.dart';
import 'package:server/components/udp_client_component.dart';
import 'package:server/components/velocity_component.dart';
import 'package:server/connection_state.dart';
import 'package:server/slot_pool.dart';
import 'package:vector_math/vector_math.dart';
import 'server_controller.dart';

class LoginRequestController extends ServerController<LoginRequest> {
  LoginRequestController() : super(MessageType.loginRequest);

  @override
  void handle(LoginRequest message, UdpClient client) {
    if (server.slotPool.noMoreSlotsLeft) {
      return server.close(client, reason: 'Server is at max capacity');
    }

    // Locking a slot beforehand.
    final slot = server.slotPool.acquire();

    final entity = world.createEntity(client.address.host)
      ..add<UdpClientComponent, UdpClient>(client)
      ..add<PositionComponent, Vector2>(Vector2(slot.value * 100, 0))
      ..add<VelocityComponent, Vector2>(Vector2.zero())
      ..add<SlotComponent, Slot>(slot);

    server.setEntity(client, entity);
    server.state.set(client, ConnectionState.play);
    client.emit(LoginSuccess(slot.value, message.username));
  }
}
