import 'package:core/messages.dart';
import 'package:core/udp_client.dart';
import 'package:server/connection_state.dart';
import 'server_controller.dart';

class HandshakeController extends ServerController<Handshake> {
  HandshakeController() : super(MessageType.handshake);

  @override
  void handle(Handshake message, UdpClient client) {
    server.state.set(client, ConnectionState.login);
  }
}
