import 'package:core/messages.dart';
import 'package:core/udp_client.dart';
import 'package:server/components/velocity_component.dart';

import 'server_controller.dart';

class PlayerControlController extends ServerController<PlayerControl> {
  PlayerControlController() : super(MessageType.playerControl);

  int previousTimestamp = 0;

  @override
  void handle(PlayerControl message, UdpClient client) {
    if (previousTimestamp > message.timestamp) {
      print('old message, ignore');
      return;
    }
    previousTimestamp = message.timestamp;
    final entity = server.getEntity(client);
    if (entity == null) {
      return;
    }

    final velocity = entity.get<VelocityComponent>()!;
    velocity.x = message.velocityX; //.clamp(-10, 10);
    velocity.y = message.velocityY; //.clamp(-10, 10);
  }
}
