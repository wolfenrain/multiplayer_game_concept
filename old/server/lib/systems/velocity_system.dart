import 'package:oxygen/oxygen.dart';
import 'package:server/components/position_component.dart';
import 'package:server/components/velocity_component.dart';

class VelocitySystem extends System {
  Query? _query;

  @override
  void init() {
    _query = createQuery([
      Has<VelocityComponent>(),
      Has<PositionComponent>(),
    ]);
  }

  @override
  void execute(double delta) {
    for (final entity in _query?.entities ?? <Entity>[]) {
      final velocity = entity.get<VelocityComponent>()!;
      final position = entity.get<PositionComponent>()!;

      position.x = (position.x! + velocity.x! * delta).clamp(0, 10000);
      position.y = (position.y! + velocity.y! * delta).clamp(0, 10000);
    }
  }
}
