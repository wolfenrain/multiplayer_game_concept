import 'package:core/messages.dart';
import 'package:oxygen/oxygen.dart';
import 'package:server/components/position_component.dart';
import 'package:server/components/slot_component.dart';
import 'package:server/components/velocity_component.dart';
import 'package:server/systems/server_system.dart';

import '../components/udp_client_component.dart';

class SocketSystem extends ServerSystem {
  Query? _query;

  @override
  void init() {
    _query = createQuery([Has<UdpClientComponent>(), Has<SlotComponent>()]);
  }

  @override
  void execute(double delta) {
    for (final mainEntity in _query?.entities ?? <Entity>[]) {
      final client = mainEntity.get<UdpClientComponent>()!;

      for (final entity in _query?.entities ?? <Entity>[]) {
        final velocity = entity.get<VelocityComponent>()!;
        final position = entity.get<PositionComponent>()!;
        final slot = entity.get<SlotComponent>()!.value!;

        client.emit(
          PlayerControl(
            playerSlot: slot.value,
            ping: server.getPing(client.value!),
            controlFlags: ControlFlags(),
            playerX: position.x!,
            playerY: position.y!,
            velocityX: velocity.x!,
            velocityY: velocity.y!,
          ),
        );
      }
    }
  }
}
