import 'package:core/messages.dart';
import 'package:oxygen/oxygen.dart';
import 'package:server/components/udp_client_component.dart';

class KeepAliveSystem extends System {
  Query? _query;

  @override
  void init() {
    _query = createQuery([Has<UdpClientComponent>()]);
  }

  @override
  void execute(double delta) {
    for (final entity in _query?.entities ?? <Entity>[]) {
      final client = entity.get<UdpClientComponent>()?.value;
      client?.emit(KeepAlive());
    }
  }
}
