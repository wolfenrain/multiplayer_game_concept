import 'package:oxygen/oxygen.dart';
import 'package:server/game_server.dart';

abstract class ServerSystem extends System {
  late final GameServer server;
}
