enum ConnectionState {
  unknown,
  handshake,
  login,
  play,
}
