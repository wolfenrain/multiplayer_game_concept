import 'dart:async';
import 'dart:io';

import 'package:core/controller.dart';
import 'package:core/messages.dart';
import 'package:core/udp_client.dart';
import 'package:core/udp_server.dart';
import 'package:dartlin/dartlin.dart';
import 'package:oxygen/oxygen.dart';
import 'package:server/components/slot_component.dart';
import 'package:server/connection_state.dart';
import 'package:server/slot_pool.dart';
import 'package:core/state_manager.dart';
import 'package:server/systems/server_system.dart';

import 'controllers/server_controller.dart';
import 'game_loop.dart';

class GameServer extends UdpServer {
  final World world = World();

  final SlotPool slotPool = SlotPool(255);

  late final GameLoop _loop;

  final state = StateManager<UdpClient, ConnectionState>(
    ConnectionState.unknown,
    logging: true,
  );

  final Map<UdpClient, Entity> _entities = {};
  final Map<UdpClient, List<int>> _pings = {};

  late final Timer _pingClearer;

  GameServer() : super(InternetAddress.anyIPv4, 8080, logging: true) {
    _loop = GameLoop(world.execute);
    _pingClearer = Timer.periodic(Duration(seconds: 1), (timer) {
      for (final client in _pings.keys) {
        _pings[client] = [];
      }
    });
  }

  @override
  void dispose() {
    _pingClearer.cancel();
    super.dispose();
  }

  Entity? getEntity(UdpClient client) => _entities[client];
  void setEntity(UdpClient client, Entity entity) {
    if (!_entities.containsKey(client)) {
      _entities[client] = entity;
      return;
    }
  }

  void removeEntity(UdpClient client) {
    if (_entities.containsKey(client)) {
      _entities.remove(client);
      return;
    }
  }

  void setPing(UdpClient client, int ping) {
    _pings[client] ??= [];
    _pings[client]?.add(ping);
  }

  int getPing(UdpClient client) {
    final total = _pings[client]?.fold<int>(0, (a, b) => a + b) ?? 0;
    if (total == 0) {
      return 0;
    }
    return total ~/ (_pings[client]?.length ?? 1);
  }

  void removePing(UdpClient client) {
    _pings.remove(client);
  }

  @override
  void onConnected(UdpClient client) {
    print('$client connected');
    state.set(client, ConnectionState.handshake);
  }

  @override
  void onDisconnected(UdpClient client) {
    print('$client disconnected');
    state.remove(client);
    removePing(client);

    final entity = getEntity(client);
    if (entity != null) {
      removeEntity(client);

      final slotComponent = entity.get<SlotComponent>();
      slotComponent?.value?.dispose();
      entity.dispose();
    }
  }

  void onTimeout(UdpClient client) => close(client, reason: 'Timed out');

  @override
  bool allowMessage(UdpClient client, Message message) {
    final clientState = state.get(client);

    return when(message, {
      isType<Handshake>(): () => clientState == ConnectionState.handshake,
      isType<LoginRequest>(): () => clientState == ConnectionState.login,
      isType<PlayerControl>(): () => clientState == ConnectionState.play,
    }).orElse(() {
      return false;
    });
  }

  @override
  void registerController(Controller controller) {
    if (controller is ServerController) {
      controller.server = this;
    }
    super.registerController(controller);
  }

  void registerSystem<T extends System>(T system) {
    if (system is ServerSystem) {
      system.server = this;
    }
    world.registerSystem<T>(system);
  }

  void registerComponent<T extends Component<V>, V>(T Function() builder) {
    world.registerComponent<T, V>(builder);
  }

  @override
  Future<void> start() async {
    world.init();
    _loop.start();
    print('Game loop started');

    return super.start();
  }
}
