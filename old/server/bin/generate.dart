import 'dart:io';

import 'package:core/messages.dart';
import 'package:core/src/payload_builder.dart';

class HeaderMessage extends Message {
  HeaderMessage() : super(MessageType(-1));

  @override
  void payload(PayloadBuilder builder) {}
}

IOSink? sink;

void text(String text) => sink?.writeln(text);

void describe(
  Message message, {
  bool withTitle = true,
  String? description,
  bool withHeaders = false,
}) {
  if (withTitle) {
    text(
      '## `${message.runtimeType}` (${(message.getLength(withHeaders: withHeaders))} bytes)',
    );
    sink?.writeln('');
    text('Message type is `0x${message.type}`');
    sink?.writeln('');
  }
  if (description != null) {
    text(description);
  }
  if (message is Acknowledge) {
    sink?.writeln('**Note**: Should be ACKed.');
    sink?.writeln();
  }
  sink?.writeln(message.describe(withHeaders: withHeaders));
}

void main() {
  final file = '${Directory.current.path}/messages.md';
  sink = File(file).openWrite();
  final header = HeaderMessage();
  describe(
    header,
    withTitle: false,
    withHeaders: true,
    description: '''
# Message header structure (${header.getLength(withHeaders: true)} bytes)

If a message contains a non null `id` it should be ACKed by sending an `Ack` with the given 
`type`.

The following table describe the header of each message.
''',
  );

  text('# General communication');
  describe(Ack(MessageType(-1)), description: '''
Whenever a message needs to be ACKed the `Ack` message needs to be send with the header `id` set 
to the `id` of the message that needs to be acknowledged and the body should contain the message 
type.

Sending an `Ack` for a message that is either unknown or not send to your client will result in a 
`FatalError` with the error `Unknown acknowledgement (<message_id>)`
''');

  describe(KeepAlive(), description: '''
The server will send out a KeepAlive message each tick to all clients, each containing a timestamp. 
If the client does not acknowledge them for over <time> seconds, the server will kick the client.
''');

  describe(FatalError(''), description: '''
Whenever a fatal error occurs this message will be send and the connected client will be 
disconnected.
''');

  text('# Handshake stage');
  text('''
## Clientbound

No clientbound message in the Handshake state.
''');

  text('## Serverbound');
  describe(Handshake(), description: '''
Send by the client to initiate a connection request.
''');

  text('''
# Login stage

The login process is as followed:
- Client -> Server: Handshake
- Server -> Client: Acknowledgement of the handshake
- Client -> Server: Login request
''');
  text('## Clientbound');
  describe(LoginSuccess(0, ''));

  text('## Serverbound');
  describe(LoginRequest(''));

  describe(
      PlayerControl(
        playerSlot: 0,
        ping: 0,
        controlFlags: ControlFlags(),
        playerX: 0,
        playerY: 0,
        velocityX: 0,
        velocityY: 0,
      ),
      description: '''
Player control oWo
''');

  sink?.close();
  print('Generated $file');
}
