import 'package:core/messages.dart';
import 'package:server/components/position_component.dart';
import 'package:server/components/slot_component.dart';
import 'package:server/components/udp_client_component.dart';
import 'package:server/components/velocity_component.dart';
import 'package:server/controllers/handshake_controller.dart';
import 'package:server/controllers/keep_alive_controller.dart';
import 'package:server/controllers/login_request_controller.dart';
import 'package:server/controllers/player_control_controller.dart';
import 'package:server/game_server.dart';
import 'package:server/systems/debug_system.dart';
import 'package:server/systems/keep_alive_system.dart';
import 'package:server/systems/socket_system.dart';
import 'package:server/systems/velocity_system.dart';

typedef MessageBuilder = void Function(Message);

void main(List<String> arguments) async {
  Message.add(MessageType.handshake, (_) => Handshake.fromPayload(_));
  Message.add(MessageType.fatalError, (_) => FatalError.fromPayload(_));

  Message.add(MessageType.loginRequest, (_) => LoginRequest.fromPayload(_));

  Message.add(MessageType.playerControl, (_) => PlayerControl.fromPayload(_));

  final server = GameServer();

  server.registerController(HandshakeController());
  server.registerController(KeepAliveController());

  server.registerController(LoginRequestController());

  server.registerController(PlayerControlController());

  server.registerSystem(DebugSystem());
  server.registerSystem(KeepAliveSystem());
  server.registerSystem(SocketSystem());
  server.registerSystem(VelocitySystem());

  server.registerComponent(() => UdpClientComponent());
  server.registerComponent(() => VelocityComponent());
  server.registerComponent(() => PositionComponent());
  server.registerComponent(() => SlotComponent());

  await server.start();
}
