#!/usr/bin/env bash

while IFS='=' read -r name value ; do
  if [[ $name == *'_MYSQL_DATABASE'* ]]; then
    prefix=${name%%_*} # delete longest match from back (everything after first _)
    user="${prefix}_MYSQL_USER"
    password="${prefix}_MYSQL_password"

    # If the database does not exist.
    if [ ! -d "/var/lib/mysql${value}" ]; then
        echo "Creating new database: ${value}"
        # Create database.
        mysql -u root -p"${MYSQL_ROOT_PASSWORD}" <<QUERY
        CREATE DATABASE ${value} DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;
        CREATE USER '${!user}'@'%' IDENTIFIED WITH mysql_native_password BY '${!password}';
        GRANT ALL PRIVILEGES ON ${value}.* to '${!user}'@'%';
        GRANT PROCESS ON *.* to '${!user}'@'%';
QUERY
    fi

  fi
done < <(env)