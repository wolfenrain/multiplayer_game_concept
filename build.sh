check_directory() {
    directory=$1 
    if [ ! -d "$directory" ] || [ ! -f "$directory/bin/server.dart" ]; then
        return 1
    fi
    return 0
}

build_directory() {
    directory=$1 

    tmp_dir=$(mktemp -d -t dart-docker-XXXXXXXXXX)
    cp ./Dockerfile "$tmp_dir/Dockerfile"
    cp -r "$directory" "$tmp_dir/server"
    cp -r "ironclad" "$tmp_dir/ironclad"
    cp -r "game_messages" "$tmp_dir/game_messages"

    cd $tmp_dir
    docker build -t "$directory:latest" .

    # rm -r $tmp_dir
} 

if [ ! -z $1 ]; then
    check_directory $1
    if [ $? -eq 0 ]; then
        build_directory $1
    else
        echo "Directory $1 is not a server"
    fi
else
    for dir in *; do
        check_directory $dir
        if [ $? -eq 0 ]; then
            echo "Building $dir"
            build_directory "$dir"
        fi
    done
fi

for dir in /tmp/dart-docker-*; do
    rm -r $dir
done