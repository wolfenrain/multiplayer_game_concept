
```mermaid
sequenceDiagram
    participant C as Client
    participant Ls as Login server
    participant Cs as Center server
    participant Ws as World server X
    C->>Ls: Login Request
    alt failed
        Ls->>C: Login failed
    else Success
        Ls->>C: Login Success
        C->>Cs: Retrieve world list
        Cs->>C: Return world list
        Note over C: User choses a world
        C->>Ws: Join world
    end

```
