import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'dart:typed_data';

import 'package:dartlin/dartlin.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/src/message/messages/ack.dart';
import 'package:ironclad/src/message/message.dart';
import 'package:ironclad/src/client/client_state.dart';
import 'package:ironclad/src/util/error_code.dart';
import 'package:ironclad/src/logger/loggable.dart';

typedef StateListener<T extends ClientState> = void Function(T state);

abstract class ClientInterface with Loggable {
  @override
  String get loggerName => '$runtimeType(${address.address}:$port)';

  /// Client version.
  String version;

  /// Address of the client.
  final InternetAddress address;

  /// Port of the client.
  final int port;

  final Map<int, int> _ackCounters = {};
  final List<Acknowledge> _ackNeeded = [];
  final Map<int, Timer> _acknowledgmentTimeouts = {};

  /// Receiving bandwidth in bytes
  int get sending => _sending;
  final List<int> _sendingBytes = [];
  int _sending = 0;

  /// Receiving bandwidth in bytes
  int get receiving => _receiving;
  final List<int> _receivingBytes = [];
  int _receiving = 0;

  late Timer _byteClearer;

  /// Retrieve all the current states of a client.
  Map<Type, ClientState> get states => UnmodifiableMapView(_states);
  final _states = <Type, ClientState>{};
  final _stateListeners = <Type, List<dynamic>>{};

  /// Retrieve all the known directors of a client.
  Map<ClientState, List<MessageDirector>> get directors =>
      UnmodifiableMapView(_directors);
  final _directors = <ClientState, List<MessageDirector>>{};

  /// When the client disconnects it will emit the reason.
  late final Stream<DisconnectReason> disconnected;
  final _disconnected = StreamController<DisconnectReason>.broadcast();

  ClientInterface(this.address, this.port, {required this.version}) {
    _byteClearer = Timer.periodic(Duration(seconds: 1), (_) {
      _sending = _sendingBytes.fold<int>(0, (a, b) => a + b);
      _receiving = _receivingBytes.fold<int>(0, (a, b) => a + b);
      _sendingBytes.clear();
      _receivingBytes.clear();
    });
    disconnected = _disconnected.stream;
  }

  void addDirector<T extends ClientState>(T state, MessageDirector director) {
    logger.debug('Adding ${director.runtimeType} for state "$state"');
    _directors[state] ??= [];
    _directors[state]?.add(director);
  }

  List<MessageDirector> getDirector<T extends ClientState>(T state) {
    return _directors[state] ?? [];
  }

  T? retrieveState<T extends ClientState>() {
    return _states[T] as T?;
  }

  /// Set the state of a certain state type.
  void state<T extends ClientState>(T state) {
    logger.debug('State changing from ${_states[T]} to $state');
    _states[T] = state;
    if (_stateListeners.containsKey(T)) {
      for (final listener in _stateListeners[T] ?? []) {
        listener(state);
      }
    }
  }

  /// Whenever a state changed of a certain type the [listener] will be called.
  void onStateChanged<T extends ClientState>(StateListener<T> listener) {
    if (!_stateListeners.containsKey(T)) {
      _stateListeners[T] = [];
    }
    _stateListeners[T]?.add(listener);
  }

  /// Dispose of the client.
  void dispose() {
    logger.debug('Disposing...');
    _disconnected.close();

    _byteClearer.cancel();

    _directors.clear();
    _states.clear();
    _stateListeners.clear();

    _ackNeeded.clear();
    _acknowledgmentTimeouts.forEach((key, value) => value.cancel());
    _acknowledgmentTimeouts.clear();
  }

  void acknowledge(Message message) {
    logger.debug('Received acknowledgement for $message');

    var handled = false;
    for (final state in List<ClientState>.from(states.values)) {
      if (!_byteClearer.isActive) {
        return;
      }
      final directors = getDirector(state);
      for (final director in directors) {
        final result = director.acknowledge(message, this);
        if (result && !handled) {
          handled = true;
        }
      }
    }

    if (!handled) {
      logger.warn('Message $message was acknowledged but not handled');
      return unhandled(message, acknowledge: true);
    }
  }

  void unacknowledged(Message message) {
    if (!_byteClearer.isActive) {
      return;
    }
    logger.debug('$message was never acknowleded');

    var handled = false;
    for (final state in List<ClientState>.from(states.values)) {
      if (!_byteClearer.isActive) {
        return;
      }
      final directors = getDirector(state);
      for (final director in directors) {
        final result = director.unacknowledged(message, this);
        if (result && !handled) {
          handled = true;
        }
      }
    }

    if (!handled) {
      logger.warn('Message $message was unacknowledged but not handled');
      return unhandled(message, unacknowledged: true);
    }
  }

  void send(Message message) {
    if (!_byteClearer.isActive) {
      return;
    }
    if (message is Acknowledge) {
      _ackCounters[message.type] = (_ackCounters[message.type] ?? 0) + 1;
      message.id = _ackCounters[message.type]!;

      _ackNeeded.add(message);
      _acknowledgmentTimeouts[message.id] = Timer(
        Duration(milliseconds: message.timeout),
        () => unacknowledged(message),
      );
    }
    final payload = Message.deconstruct(message);
    _sendingBytes.add(payload.lengthInBytes);

    logger.debug('Sending $message (${payload.lengthInBytes} bytes)');

    return onSend(payload);
  }

  void onSend(Uint8List payload);

  void unhandled(
    Message message, {
    bool acknowledge = false,
    bool unacknowledged = false,
  });

  void receive(Message message) {
    final payload = Message.deconstruct(message);
    _receivingBytes.add(payload.lengthInBytes);
    logger.debug('Received $message (${payload.lengthInBytes} bytes)');

    if (message is Ack) {
      final ackMessage = _ackNeeded.firstOrNull(
        (m) => m.id == message.id,
      );
      if (ackMessage == null) {
        logger.warn('Received unknown acknowledgement (id: ${message.id}');
        return disconnect(
          errorCode: ErrorCode.fatalError,
          reason: 'Unknown acknowledgement',
        );
      }
      _ackNeeded.remove(ackMessage);
      _acknowledgmentTimeouts.remove(ackMessage.id)?.cancel();

      return acknowledge(ackMessage);
    }

    var handled = false;
    for (final state in List<ClientState>.from(states.values)) {
      if (!_byteClearer.isActive) {
        return;
      }
      final directors = getDirector(state);
      for (final director in directors) {
        final result = director.handle(message, this);
        if (result && !handled) {
          handled = true;
        }
      }
    }

    if (handled) {
      if (message is! Acknowledge) {
        return;
      }
      return send(Ack(message.id));
    } else {
      return unhandled(message);
    }
  }

  void onDisconnect(ErrorCode errorCode, String reason);

  /// Disconnect client.
  ///
  /// Will also [dispose] of the client.
  void disconnect({
    ErrorCode errorCode = ErrorCode.unknown,
    String reason = 'unknown',
    bool callOnDisconnect = true,
  }) {
    logger.info('Disconnecting with reason: $reason');
    _disconnected.add(DisconnectReason._(errorCode, reason));
    if (callOnDisconnect) {
      onDisconnect(errorCode, reason);
    }
    dispose();
  }

  @override
  bool operator ==(Object object) {
    if (object is ClientInterface) {
      return address.address == object.address.address && port == object.port;
    }
    return false;
  }
}

class DisconnectReason {
  final ErrorCode errorCode;

  final String reason;

  DisconnectReason._(this.errorCode, this.reason);
}
