/// State class used for defining different states on a client.
abstract class ClientState {
  /// Unique value of the state.
  final int value;

  const ClientState(this.value);

  @override
  String toString() => '$runtimeType($value)';
}

/// Describe the connection state of a client.
class ConnectionState extends ClientState {
  static const initial = ConnectionState(0);
  static const anonymous = ConnectionState(1);
  static const established = ConnectionState(2);

  const ConnectionState(int value) : super(value);
}
