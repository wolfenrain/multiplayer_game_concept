import 'package:ironclad/src/client/client.dart';
import 'package:ironclad/src/message/messages/client_heartbeat.dart';
import 'package:ironclad/src/message/message_handler.dart';
import 'package:ironclad/util.dart';

class ClientHeartbeatHandler extends MessageHandler<ClientHeartbeat, Client> {
  @override
  void acknowledge(ClientHeartbeat message, Client client) {}

  @override
  void unacknowledged(ClientHeartbeat message, Client client) {
    client.disconnect(errorCode: ErrorCode.timeout, reason: 'Timeout');
  }
}
