import 'package:ironclad/src/client/client.dart';
import 'package:ironclad/src/message/messages/client_hello_resp.dart';
import 'package:ironclad/src/message/message_handler.dart';

class ClientHelloRespHandler extends MessageHandler<ClientHelloResp, Client> {
  @override
  void handle(ClientHelloResp message, Client client) {
    client.hello();
  }
}
