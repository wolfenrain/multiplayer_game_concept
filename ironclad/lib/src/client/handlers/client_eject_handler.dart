import 'package:ironclad/src/client/client.dart';
import 'package:ironclad/src/message/messages/client_eject.dart';
import 'package:ironclad/src/message/message_handler.dart';

class ClientEjectHandler extends MessageHandler<ClientEject, Client> {
  @override
  void handle(ClientEject message, Client client) {
    client.logger.warn('Client was eject: $message');
    // TODO: propogate this upwards?
    client.disconnect(
      errorCode: message.errorCode,
      reason: message.reason,
      callOnDisconnect: false,
    );
  }
}
