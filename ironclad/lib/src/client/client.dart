import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:ironclad/message.dart';
import 'package:ironclad/src/client/handlers/client_eject_handler.dart';
import 'package:ironclad/src/client/handlers/client_heartbeat_handler.dart';
import 'package:ironclad/src/client/handlers/client_hello_resp_handler.dart';
import 'package:ironclad/src/logger/log_level.dart';
import 'package:ironclad/src/message/messages/client_disconnect.dart';
import 'package:ironclad/src/message/messages/client_heartbeat.dart';
import 'package:ironclad/src/message/messages/client_hello.dart';
import 'package:ironclad/src/message/message.dart';
import 'package:ironclad/src/client/client_interface.dart';
import 'package:ironclad/src/client/client_state.dart';
import 'package:ironclad/src/util/error_code.dart';
import 'package:ironclad/src/logger/loggable.dart';

class Client extends ClientInterface with Loggable {
  RawDatagramSocket? _socket;
  Timer? _heartbeat;
  Timer? _clientHelloTimeout;

  Client(
    InternetAddress address,
    int port, {
    required String version,
    LogLevel logLevel = LogLevel.info,
  }) : super(address, port, version: version) {
    LogLevel.main = logLevel;
    Message.defaultRegister();

    logger.debug('Setting up client for ${this.address.host}:${this.port}');

    onStateChanged<ConnectionState>((state) {
      if (state == ConnectionState.anonymous) {
        _heartbeat = Timer.periodic(
          Duration(seconds: 5),
          (_) => send(ClientHeartbeat()),
        );
      }
    });

    final defaultDirector = MessageDirector()
      ..attach(ClientEjectHandler())
      ..attach(ClientHeartbeatHandler());

    addDirector(
      ConnectionState.initial,
      MessageDirector()
        ..attach(ClientHelloRespHandler())
        ..attach(ClientEjectHandler()),
    );
    addDirector(ConnectionState.anonymous, defaultDirector);
    addDirector(ConnectionState.established, defaultDirector);
  }

  @override
  void dispose() {
    _heartbeat?.cancel();
    _heartbeat = null;

    _clientHelloTimeout?.cancel();
    _clientHelloTimeout = null;

    _socket?.close();
    _socket = null;

    super.dispose();
  }

  @override
  void onSend(Uint8List payload) {
    // TODO: throw error if socket is null?
    _socket?.send(payload, address, port);
  }

  @override
  void unhandled(
    Message message, {
    bool acknowledge = false,
    bool unacknowledged = false,
  }) {
    // // TODO: how to handle unhandled messages.
    // throw Exception('$message is not handled');
    logger.debug('$message is not handled');
  }

  //#region Connect
  /// Connect to the remote server.
  Future<void> connect() async {
    if (_socket != null) return;

    _socket = await _getSocket();
    _socket?.listen(_onEvent);

    // Set initial connection state.
    state(ConnectionState.initial);

    // Send HELLO.
    send(ClientHello(version));

    _clientHelloTimeout = Timer(
      Duration(milliseconds: 10000), // TODO: time configurable
      () => disconnect(
        errorCode: ErrorCode.noHelloResponse,
        reason: 'No HELLO response received',
        callOnDisconnect: false,
      ),
    );
  }

  /// Hello has been responded.
  ///
  /// **NOTE**: Only used for internal communication, do not use.
  void hello() {
    if (_clientHelloTimeout != null) {
      _clientHelloTimeout?.cancel();
      _clientHelloTimeout = null;
      state(ConnectionState.anonymous);
    }
  }

  @override
  void onDisconnect(ErrorCode errorCode, String reason) {
    send(ClientDisconnect());
  }

  Future<RawDatagramSocket> _getSocket() async {
    RawDatagramSocket? socket;
    var port = 8081;
    await Future.doWhile(() async {
      try {
        socket = await RawDatagramSocket.bind(
          InternetAddress.anyIPv4,
          port++,
          reuseAddress: false,
        );
        return false;
      } catch (err) {
        return true;
      }
    });
    logger.info('Client listening on ${socket?.address.address}:$port');

    return socket!;
  }

  void _onEvent(RawSocketEvent event) {
    if (event == RawSocketEvent.read) {
      final dg = _socket?.receive();
      if (dg == null) return;

      if (dg.address != address || dg.port != port) {
        // TODO: handle from wrong address.
        return logger.error(
          'received message from unknown address: ${dg.address.host}:${dg.port}',
        );
      }

      try {
        final message = Message.construct(dg.data);
        if (message == null) {
          // TODO: message is null.
          return logger.debug('TODO message is null');
        }
        return receive(message);
      } on MessageError catch (err) {
        // TODO: handle error.
        return logger.debug('TODO: handle error: $err');
      }
    }
  }
  //#endregion
}
