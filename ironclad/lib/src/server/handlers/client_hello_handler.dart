import 'package:ironclad/src/message/messages/client_hello_resp.dart';
import 'package:ironclad/src/message/messages/client_hello.dart';
import 'package:ironclad/src/message/message_handler.dart';
import 'package:ironclad/src/client/client_state.dart';
import 'package:ironclad/src/server/remote_client.dart';

class ClientHelloHandler extends MessageHandler<ClientHello, RemoteClient> {
  @override
  void handle(ClientHello message, RemoteClient connection) {
    connection.version = message.version;
    connection.state(ConnectionState.anonymous);
    connection.send(ClientHelloResp());
  }
}
