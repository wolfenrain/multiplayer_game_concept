import 'package:ironclad/src/message/messages/client_heartbeat.dart';
import 'package:ironclad/src/message/message_handler.dart';
import 'package:ironclad/src/server/remote_client.dart';

class ClientHeartbeatHandler
    extends MessageHandler<ClientHeartbeat, RemoteClient> {
  @override
  void handle(ClientHeartbeat message, RemoteClient connection) {
    connection.heartbeat();
  }
}
