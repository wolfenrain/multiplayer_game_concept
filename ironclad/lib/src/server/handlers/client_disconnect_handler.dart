import 'package:ironclad/src/message/messages/client_disconnect.dart';
import 'package:ironclad/src/message/message_handler.dart';
import 'package:ironclad/src/server/remote_client.dart';

class ClientDisconnectHandler
    extends MessageHandler<ClientDisconnect, RemoteClient> {
  @override
  void handle(ClientDisconnect message, RemoteClient connection) {
    connection.logger.info('Is disconnecting');
    connection.dispose();
  }
}
