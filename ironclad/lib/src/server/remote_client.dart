import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:ironclad/server.dart';
import 'package:ironclad/src/client/client_interface.dart';
import 'package:ironclad/src/client/client_state.dart';
import 'package:ironclad/src/message/messages/client_eject.dart';
import 'package:ironclad/src/server/server.dart';
import 'package:ironclad/src/util/error_code.dart';
import 'package:ironclad/src/message/message.dart';

class RemoteClient extends ClientInterface {
  Timer? _heartbeatTimeout;

  /// Server that the client is connected to.
  final Server server;
  final void Function(RemoteClient client) _onDisconnect;

  RemoteClient(
    InternetAddress address,
    int port,
    this._onDisconnect, {
    required this.server,
  }) : super(address, port, version: 'unknown');

  @override
  void onDisconnect(ErrorCode errorCode, String reason) {
    _onDisconnect(this);
    send(ClientEject(errorCode, reason));
  }

  @override
  void onSend(Uint8List payload) {
    server.send(this, payload);
  }

  @override
  void unhandled(
    Message message, {
    bool acknowledge = false,
    bool unacknowledged = false,
  }) {
    if (acknowledge) {
      return disconnect(
        errorCode: ErrorCode.fatalError,
        reason: 'Acknowledgement was not handled by server',
      );
    }
    if (unacknowledged) {
      return disconnect(
        errorCode: ErrorCode.fatalError,
        reason: 'Unacknowledgement was not handled by server',
      );
    }
    return disconnect(
      errorCode: ErrorCode.fatalError,
      reason: 'Message was not handled by server',
    );
  }

  @override
  void dispose() {
    state(ConnectionState.initial);

    _heartbeatTimeout?.cancel();

    super.dispose();
  }

  /// Heartbeat received, will set a new one.
  void heartbeat() {
    _heartbeatTimeout?.cancel();
    // TODO: make config
    _heartbeatTimeout = Timer(Duration(seconds: 30), () {
      disconnect(errorCode: ErrorCode.timeout, reason: 'Timeout');
    });
  }
}
