import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'dart:typed_data';

import 'package:dartlin/dartlin.dart';
import 'package:ironclad/logger.dart';
import 'package:ironclad/message.dart';

import 'package:ironclad/server.dart';
import 'package:ironclad/src/client/client_state.dart';
import 'package:ironclad/src/client/handlers/client_heartbeat_handler.dart';
import 'package:ironclad/src/message/message.dart';
import 'package:ironclad/src/server/handlers/client_disconnect_handler.dart';
import 'package:ironclad/src/server/handlers/client_hello_handler.dart';
import 'package:ironclad/src/server/remote_client.dart';
import 'package:ironclad/util.dart';

class Server with Loggable {
  RawDatagramSocket? _socket;

  /// List of connected clients.
  List<RemoteClient> get clients => UnmodifiableListView(_clients);
  final List<RemoteClient> _clients = [];

  /// List of [MessageDirector]s.
  Map<ClientState, List<MessageDirector>> get directors =>
      UnmodifiableMapView(_directors);
  final _directors = <ClientState, List<MessageDirector>>{};

  /// Max allowed clients that can connect to the [Server].
  final int maxClients;

  /// Public address to which the [Server] listens to.
  late final InternetAddress publicAddress;

  /// Port on which the [Server] listens to.
  late final int port;

  /// Emits a [RemoteClient] when it connects.
  late final Stream<RemoteClient> clientConnected;
  final _clientConnected = StreamController<RemoteClient>.broadcast();

  /// Emits a [RemoteClient] when it disconnects.
  late final Stream<RemoteClient> clientDisconnected;
  final _clientDisconnected = StreamController<RemoteClient>.broadcast();

  Server(
    this.maxClients, {
    InternetAddress? publicAddress,
    int? port,
    LogLevel logLevel = LogLevel.info,
  }) {
    LogLevel.main = logLevel;
    Message.defaultRegister();

    this.publicAddress =
        publicAddress ?? env('NET_SERVER_HOST', InternetAddress('127.0.0.1'));
    this.port = port ?? env('NET_SERVER_PORT', 8080);

    logger.debug(
      'Setting up server for ${this.publicAddress.host}:${this.port}',
    );

    clientConnected = _clientConnected.stream;
    clientDisconnected = _clientDisconnected.stream;
    clientConnected.listen(_clients.add);
    clientDisconnected.listen(_clients.remove);

    final defaultDirector = MessageDirector()
      ..attach(ClientDisconnectHandler())
      ..attach(ClientHeartbeatHandler());

    addDirector(
      ConnectionState.initial,
      MessageDirector()..attach(ClientHelloHandler()),
    );
    addDirector(ConnectionState.anonymous, defaultDirector);
    addDirector(ConnectionState.established, defaultDirector);
  }

  /// Send a binary payload the [client].
  void send(RemoteClient client, Uint8List payload) {
    _socket?.send(payload, client.address, client.port);
  }

  /// Disconnect the [client] with given [reason] and [errorCode].
  void disconnect(
    RemoteClient client, {
    ErrorCode errorCode = ErrorCode.unknown,
    String reason = 'unknown',
  }) {
    client.disconnect(errorCode: errorCode, reason: reason);
  }

  void addDirector<T extends ClientState>(T state, MessageDirector director) {
    logger.debug('Adding ${director.runtimeType} for state "$state"');
    _directors[state] ??= [];
    _directors[state]?.add(director);
  }

  /// Returns a list of [RemoteClient]s of the given [state].
  List<RemoteClient> clientsWithState<T extends ClientState>(T state) {
    return _clients.where((c) => c.states[T] == state).toList();
  }

  /// Start the server.
  Future<void> start() async {
    _socket = await RawDatagramSocket.bind(
      publicAddress,
      port,
      reuseAddress: false,
    )
      ..listen(_onEvent);

    logger.info('listening on ${publicAddress.host}:$port');
  }

  void _onEvent(RawSocketEvent event) {
    if (event == RawSocketEvent.read) {
      final dg = _socket?.receive();
      if (dg == null) return;

      final address = dg.address;
      final port = dg.port;

      var client = _clients.firstOrNull(
        (c) => c.address == address && c.port == port,
      );
      if (client == null) {
        client = RemoteClient(
          address,
          port,
          _clientDisconnected.add,
          server: this,
        );
        if (_clients.length >= maxClients) {
          return client.disconnect(reason: 'Server is full');
        }
        client.state(ConnectionState.initial);

        // Setup heartbeat timer.
        client.onStateChanged<ConnectionState>((state) {
          if (state == ConnectionState.anonymous) {
            client?.heartbeat();
          }
        });

        // Register all directors to the client.
        for (final pair in directors.entries) {
          for (final director in pair.value) {
            client.addDirector(pair.key, director);
          }
        }

        _clientConnected.add(client);
      }

      try {
        final message = Message.construct(dg.data);
        if (message == null) {
          logger.debug('TODO message is null');
          return client.disconnect();
        }
        return client.receive(message);
      } on MessageError catch (err) {
        return client.disconnect(
          errorCode: ErrorCode.fatalError,
          reason: err.message,
        );
      }
    }
  }

  /// Stop the server.
  Future<void> stop() async {
    logger.info('Stopping...');
    for (final client in clients) {
      client.disconnect(reason: 'Server shutting down');
    }

    await _clientConnected.close();
    await _clientDisconnected.close();

    _socket?.close();
  }
}
