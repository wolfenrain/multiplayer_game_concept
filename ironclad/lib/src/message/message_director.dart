import 'package:ironclad/src/logger/loggable.dart';
import 'package:ironclad/src/message/message.dart';
import 'package:ironclad/src/message/message_handler.dart';
import 'package:ironclad/src/client/client_interface.dart';

class MessageDirector with Loggable {
  final _handlers = <Type, List<MessageHandler>>{};

  List<MessageHandler> _getHandlers(Type type) {
    if (!_handlers.containsKey(type)) {
      return _handlers[type] = [];
    }
    return List<MessageHandler>.from(_handlers[type]!);
  }

  bool handle<T extends Message>(T message, ClientInterface client) {
    final handlers = _getHandlers(message.runtimeType);
    for (final handler in handlers) {
      handler.handle(message, client);
    }
    return handlers.isNotEmpty;
  }

  bool acknowledge<T extends Message>(
    T message,
    ClientInterface client,
  ) {
    final handlers = _getHandlers(message.runtimeType);
    for (final handler in handlers) {
      handler.acknowledge(message, client);
    }
    return handlers.isNotEmpty;
  }

  bool unacknowledged<T extends Message>(
    T message,
    ClientInterface client,
  ) {
    final handlers = _getHandlers(message.runtimeType);
    for (final handler in handlers) {
      handler.unacknowledged(message, client);
    }
    return handlers.isNotEmpty;
  }

  void attach<T extends Message, V extends ClientInterface>(
    MessageHandler<T, V> handler,
  ) {
    if (!_handlers.containsKey(T)) {
      _handlers[T] = [];
    }
    if (!_handlers[T]!.contains(handler)) {
      logger.debug('Attaching message handler for message "$T"');
      _handlers[T]!.add(handler);
    }
  }

  void detach<T extends Message, V extends ClientInterface>(
    MessageHandler<T, V> handler,
  ) {
    if (!_handlers.containsKey(T)) {
      return;
    }
    logger.debug('Detaching message handler for message "$T"');
    _handlers[T]?.remove(handler);
  }
}
