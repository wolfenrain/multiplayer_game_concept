import 'dart:typed_data';

import 'package:binarize/binarize.dart';
import 'package:dartlin/dartlin.dart';
import 'package:ironclad/src/message/messages/ack.dart';
import 'package:ironclad/src/message/messages/client_disconnect.dart';
import 'package:ironclad/src/message/messages/client_heartbeat.dart';
import 'package:ironclad/src/message/messages/client_hello.dart';
import 'package:ironclad/src/message/messages/client_eject.dart';
import 'package:ironclad/src/message/messages/client_hello_resp.dart';

class MessageError<T extends Message> {
  final String message;

  const MessageError(this.message);

  @override
  String toString() => 'MessageError on $T: $message';
}

mixin Acknowledge on Message {
  late int id;

  /// Timeout in miliseconds.
  int get timeout;

  @override
  String toString() => '$runtimeType<Acknowledge>(type: $type)';
}

class _RegisteredMessage<T extends Message> {
  static final _lengthOffset = uint16.length(0);

  int get lengthOffset => (isAcknowledge ? _lengthOffset : 0) + _lengthOffset;

  final Type type;
  final bool isAcknowledge;
  final MessageArguments<T> _arguments;
  Map<Function, PayloadType> get arguments => {..._arguments};

  final Function constructor;

  _RegisteredMessage(
    this.type,
    this.isAcknowledge,
    this._arguments,
    this.constructor,
  );
}

typedef MessageArguments<T extends Message>
    = Map<dynamic Function(T message), PayloadType>;

abstract class Message {
  /// Type of the message, should be unique.
  int get type;

  @override
  String toString() {
    final arguments = Message._registeredMessages[type]!.arguments;
    final data = arguments.keys.map((mapper) => mapper(this)).map((e) {
      return e is String ? '"$e"' : e;
    });
    return '$runtimeType(${data.join(', ')})';
  }

  static final _registeredMessages = <int, _RegisteredMessage>{};

  static bool _registered = false;

  static void defaultRegister() {
    if (_registered) {
      return;
    }
    _registered = true;
    Message.register<Ack>(Ack.arguments, Ack.constructor);
    Message.register<ClientHello>(
      ClientHello.arguments,
      ClientHello.constructor,
    );
    Message.register<ClientHelloResp>(
      ClientHelloResp.arguments,
      ClientHelloResp.constructor,
    );
    Message.register<ClientDisconnect>(
      ClientDisconnect.arguments,
      ClientDisconnect.constructor,
    );
    Message.register<ClientEject>(
      ClientEject.arguments,
      ClientEject.constructor,
    );
    Message.register<ClientHeartbeat>(
      ClientHeartbeat.arguments,
      ClientHeartbeat.constructor,
    );
  }

  /// Register a message and how it should be constructed and deconstructed.
  static void register<T extends Message>(
    MessageArguments<T> arguments,
    Function constructor,
  ) {
    final testPayload = Payload.read(Uint8List(65535));

    // Testing the constructor.
    final message = _construct(constructor, arguments.values, testPayload);
    if (message is! T) {
      throw MessageError<T>('Builder does not return instance of $T');
    }

    // Testing the deconstruction.
    final data = [];
    arguments.forEach((key, value) => data.add(key(message)));

    // TODO: check still needed?
    if (data.length != arguments.length) {
      throw MessageError<T>(
        'Length of the deconstructed list should match the arguments for $T',
      );
    }

    // Confirming that deconstructor returned correct values.
    if (data
        .mapIndexed((i, d) => arguments.values.elementAt(i).isType(d))
        .any((d) => !d)) {
      throw MessageError<T>(
        'Types of the deconstructed list should match the arguments for $T',
      );
    }

    // Register the constructor, deconstructor and arguments for this type.
    final type = message.type;
    if (_registeredMessages[type] != null) {
      throw MessageError<T>(
        'Message ${_registeredMessages[type]!.type} is already using a type value of $type',
      );
    }

    _registeredMessages[type] = _RegisteredMessage<T>(
      T,
      message is Acknowledge,
      arguments,
      constructor,
    );
  }

  /// Construct a message based on the given payload.
  ///
  /// If the payload was malformed in any way an error will be thrown.
  /// If no type was found for the given payload the returned value will be null.
  static T? construct<T extends Message>(Uint8List data) {
    final payload = Payload.read(data);
    final length = payload.get(uint16);
    final type = payload.get(uint16);

    if (_registeredMessages[type] == null) {
      return null;
    }
    final registeredMessage = _registeredMessages[type]!;

    if (payload.length - registeredMessage.lengthOffset != length) {
      throw MessageError<T>(
        'Given payload length(${payload.length - registeredMessage.lengthOffset}) does not match given length ($length)',
      );
    }

    final body = Payload.read(
      data.skip(2 + registeredMessage.lengthOffset).take(length),
    );

    try {
      final message = _construct(
        registeredMessage.constructor,
        registeredMessage.arguments.values,
        body,
      );
      if (message is Acknowledge) {
        message.id = payload.get(uint16);
      }
      return message as T?;
    } catch (err) {
      throw MessageError<T>('$err');
    }
  }

  /// Deconstuct a given message into bytes.
  static Uint8List deconstruct(Message message) {
    if (_registeredMessages[message.type] == null) {
      throw Exception('${message.runtimeType} is not registered');
    }
    final registeredMessage = _registeredMessages[message.type]!;

    final arguments = registeredMessage.arguments;
    final body = Payload.write();
    arguments.forEach((mapper, type) => body.set(type, mapper(message)));
    final bodyData = binarize(body);

    final typeOffset = uint16.length(message.type);
    final header = Payload.write();
    header.set(uint16, bodyData.lengthInBytes + typeOffset);
    header.set(uint16, message.type);
    if (message is Acknowledge) {
      header.set(uint16, message.id);
    }

    final headerData = binarize(header);

    return Uint8List.fromList([...headerData, ...bodyData]);
  }

  static T? _construct<T extends Message>(
    Function constructor,
    Iterable<PayloadType> arguments,
    PayloadReader payload,
  ) {
    return Function.apply(
      constructor,
      arguments.map((a) => payload.get(a)).toList(),
    );
  }
}
