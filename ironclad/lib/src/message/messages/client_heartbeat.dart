import 'package:ironclad/src/message/message.dart';

class ClientHeartbeat extends Message with Acknowledge {
  static final MessageArguments<ClientHeartbeat> arguments = {};
  static ClientHeartbeat constructor() {
    return ClientHeartbeat();
  }

  @override
  int get type => 5;

  @override
  int get timeout => 15000; // TODO: configurable

}
