import 'package:ironclad/src/message/message.dart';

class ClientHelloResp extends Message {
  static final MessageArguments<ClientHelloResp> arguments = {};
  static ClientHelloResp constructor() => ClientHelloResp();

  @override
  int get type => 2;
}
