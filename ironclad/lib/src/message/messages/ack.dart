import 'package:binarize/binarize.dart';
import 'package:ironclad/message.dart';

class Ack extends Message {
  static final MessageArguments<Ack> arguments = {
    (message) => message.id: uint32,
  };
  static Ack constructor(int id) => Ack(id);

  @override
  int get type => 0;

  final int id;

  Ack(this.id);
}
