import 'package:ironclad/src/message/message.dart';

class ClientDisconnect extends Message {
  static final MessageArguments<ClientDisconnect> arguments = {};
  static ClientDisconnect constructor() => ClientDisconnect();

  @override
  int get type => 3;
}
