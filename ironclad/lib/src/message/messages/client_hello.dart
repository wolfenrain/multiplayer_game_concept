import 'package:binarize/binarize.dart';
import 'package:ironclad/src/message/message.dart';

class ClientHello extends Message {
  static final MessageArguments<ClientHello> arguments = {
    (message) => message.version: string,
  };
  static ClientHello constructor(String version) => ClientHello(version);

  @override
  int get type => 1;

  final String version;

  ClientHello(this.version);
}
