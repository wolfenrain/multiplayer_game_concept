import 'package:binarize/binarize.dart';
import 'package:ironclad/src/message/message.dart';
import 'package:ironclad/src/util/error_code.dart';

class ClientEject extends Message {
  static final MessageArguments<ClientEject> arguments = {
    (message) => message.errorCode.value: uint16,
    (message) => message.reason: string,
  };
  static ClientEject constructor(int errorCode, String reason) {
    return ClientEject(ErrorCode(errorCode), reason);
  }

  @override
  int get type => 4;

  final ErrorCode errorCode;
  final String reason;

  ClientEject(this.errorCode, this.reason);
}
