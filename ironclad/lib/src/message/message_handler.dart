import 'package:ironclad/src/message/message.dart';
import 'package:ironclad/src/client/client_interface.dart';

// TODO: Create abstraction for both client and connection
abstract class MessageHandler<T extends Message, V extends ClientInterface> {
  const MessageHandler();

  void handle(T message, V client) {
    throw Exception('Message $T was not handled by $runtimeType');
  }

  void acknowledge(T message, V client) {
    throw Exception('Message $T was not acknowledged by $runtimeType');
  }

  void unacknowledged(T message, V client) {
    throw Exception('Message $T was not unacknowledged by $runtimeType');
  }
}
