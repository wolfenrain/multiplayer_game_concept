class ErrorCode {
  static const unknown = ErrorCode(0);
  static const fatalError = ErrorCode(1);
  static const timeout = ErrorCode(2);
  static const noHelloResponse = ErrorCode(3);

  final int value;

  const ErrorCode(this.value);

  @override
  String toString() => '$value';
}
