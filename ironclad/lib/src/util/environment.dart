import 'dart:io';

import 'package:dartlin/dartlin.dart';
import 'package:ironclad/logger.dart';

const _logger = Logger('Environment');

T env<T>(String key, [T? fallback]) {
  _logger.debug('Retrieving environment variable "$key"');

  final value = Platform.environment[key];
  if (value == null) {
    if (fallback != null) {
      _logger.warn(
        'Environment variable "$key" not found, falling back to "$fallback"',
      );
      return fallback;
    }
    throw Exception('No environment variable found for "$key"');
  }
  return when(T, {
    int: () => int.parse(value),
    String: () => value,
    double: () => double.parse(value),
    InternetAddress: () => InternetAddress(value),
    LogLevel: () => LogLevel.fromString(value),
  }).orElse(() => throw Exception('No mapper found for type $T')) as T;
}
