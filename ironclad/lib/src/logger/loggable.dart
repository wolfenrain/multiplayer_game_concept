import 'package:ironclad/src/logger/logger.dart';

mixin Loggable {
  String get loggerName => '$runtimeType';

  /// Logger on a given class.
  Logger get logger => Logger(loggerName);
}
