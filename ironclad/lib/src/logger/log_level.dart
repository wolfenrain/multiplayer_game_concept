import 'package:dartlin/dartlin.dart';

class LogLevel {
  static LogLevel main = debug;

  static const debug = LogLevel(400);
  static const info = LogLevel(300);
  static const warning = LogLevel(200);
  static const error = LogLevel(100);

  final int value;

  const LogLevel(this.value);

  @override
  String toString() => 'LogLevel($value)';

  static LogLevel fromString(String level) {
    return when(level.toLowerCase(), {
      'debug': () => debug,
      'warning': () => warning,
      'error': () => error,
    }).orElse(() => info);
  }
}
