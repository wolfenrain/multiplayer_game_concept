import 'package:colored_print/colored_print.dart';
import 'package:ironclad/logger.dart';

/// Simple logger class.
class Logger {
  /// Context of the logger.
  final String name;

  const Logger(this.name);

  /// Debug logging.
  void debug(Object message) {
    if (LogLevel.main.value < LogLevel.debug.value) {
      return;
    }
    return _log(message, PrintColor.cyan);
  }

  /// Info logging.
  void info(Object message) {
    if (LogLevel.main.value < LogLevel.info.value) {
      return;
    }
    return _log(message, PrintColor.white);
  }

  /// Warning logging.
  void warn(Object message) {
    if (LogLevel.main.value < LogLevel.warning.value) {
      return;
    }
    return _log(message, PrintColor.yellow);
  }

  /// Error logging.
  void error(Object message) {
    if (LogLevel.main.value < LogLevel.error.value) {
      return;
    }
    return _log(message, PrintColor.red);
  }

  void _log(Object message, PrintColor color) {
    return ColoredPrint.show('[$name] $message', messageColor: color);
  }
}
