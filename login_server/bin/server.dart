import 'package:game_messages/login_messages.dart';
import 'package:login_server/src/entity/entity.dart';
import 'package:login_server/src/users/user_entity.dart';
import 'package:login_server/src/users/user_repository.dart';

void main(List<String> arguments) async {
  registerMessages();

  Entity.register(User.fields, User.constructor);

  final x = await UserRepository().getByUsername('Hi');

  // final server = LoginServer();
  // server.run();
}
