import 'package:ironclad/client.dart';
import 'package:ironclad/util.dart';
import 'package:login_server/src/net/login/login_request_handler.dart';
import 'package:ironclad/logger.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/server.dart';

class LoginServer with Loggable {
  late final Server _server;

  LoginServer() {
    LogLevel.main = env('LOG_LEVEL', LogLevel.debug);
  }

  void _setupNetServer() {
    _server = Server(
      255,
      logLevel: LogLevel.main,
    )..addDirector(
        ConnectionState.anonymous,
        MessageDirector()..attach(LoginRequestHandler()),
      );
  }

  void run() {
    _setupNetServer();

    _server.start();
  }
}
