import 'package:game_messages/login_messages.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/server.dart';
import 'package:login_server/src/users/user_repository.dart';

class LoginRequestHandler extends MessageHandler<LoginRequest, RemoteClient> {
  final userRepository = UserRepository();

  @override
  void handle(LoginRequest message, RemoteClient client) async {
    if (false) {
      // TODO: add reason
      return client.send(LoginFailure());
    }
    final user = await userRepository.getByUsername(message.username);

    await Future.delayed(Duration(seconds: 2));

    client.state(ConnectionState.established);
    return client.send(LoginSuccess());
  }
}
