import 'dart:io';

import 'package:ironclad/util.dart';
import 'package:mysql1/mysql1.dart';
import 'package:mysql1/src/single_connection.dart';

void x() async {
  final conn = await MySqlConnection.connect(ConnectionSettings(
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'root',
    db: 'database',
  ));
}

class Connection {
  MySqlConnection? _connection;

  Connection._();

  Future<void> connect() async {
    _connection = await MySqlConnection.connect(ConnectionSettings(
      host: env('MYSQL_HOST', 'localhost'),
      port: env('MYSQL_PORT', 3306),
      user: env<String>('MYSQL_USER'),
      password: env<String>('MYSQL_PASS'),
      db: env<String>('MYSQL_DATA'),
    ));
  }

  Future<Results> query(String sql, [List<Object?>? values]) {
    return _connection!.query(sql, values);
  }

  static Connection? _instance;

  static Future<Connection> get() async {
    if (_instance != null) {
      return _instance!;
    }
    _instance = Connection._();
    await _instance?.connect();
    return _instance!;
  }

  Future<void> transaction(
    void Function(TransactionContext context) queryBlock,
  ) {
    return this.transaction(queryBlock);
  }
}
