import 'package:login_server/src/entity/repository.dart';
import 'package:login_server/src/users/user_entity.dart';

class UserRepository extends Repository<User> {
  Future<User?> getByUsername(String username) {
    return getBy(['username'], [username]);
  }
}
