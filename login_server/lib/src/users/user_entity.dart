import 'package:login_server/src/entity/entity.dart';

class User extends Entity {
  static final EntityFields<User> fields = {
    'id': (user) => user.id,
    'username': (user) => user.username,
    'password': (user) => user.password,
  };

  static User constructor(int id, String username, String password) {
    return User(id: id, username: username, password: password);
  }

  int? id;

  String? username;

  String? password;

  User({
    this.id,
    this.username,
    this.password,
  });
}
