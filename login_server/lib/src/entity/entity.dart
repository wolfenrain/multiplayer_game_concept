// import 'package:mysql1/mysql1.dart';

import 'dart:collection';

class _RegisteredEntity<T extends Entity> {
  final Map<String, dynamic Function(T entity)> _fields;
  Map<String, Function> get fields => {..._fields};

  final Function constructor;

  _RegisteredEntity(this._fields, this.constructor);
}

typedef EntityFields<T extends Entity>
    = Map<String, dynamic Function(T entity)>;

class Entity {
  static final _entities = <Type, _RegisteredEntity>{};
  static Map<Type, _RegisteredEntity> get entities {
    return UnmodifiableMapView(_entities);
  }

  static void register<T extends Entity>(
    Map<String, dynamic Function(T model)> fields,
    Function constructor,
  ) {
    _entities[T] = _RegisteredEntity<T>(fields, constructor);
  }
}
