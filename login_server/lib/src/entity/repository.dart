import 'package:login_server/src/database/connection.dart';
import 'package:login_server/src/entity/entity.dart';
import 'package:plural_noun/plural_noun.dart';

class Repository<T extends Entity> {
  String _toPlural(String s) =>
      PluralRules().convertToPluralNoun(s).toLowerCase();

  String _saveString(String s) => '`$s`';

  Future<T?> getBy(List<String> fields, List<dynamic> values) async {
    final entity = Entity.entities[T];
    if (entity == null) {
      throw Exception('Entity $T is not registered');
    }
    if (fields.any((f) => !entity.fields.containsKey(f))) {
      throw Exception(
        'Given fields (${fields.join(', ')}) do not confirm to known fields on $T: (${entity.fields.keys.join(', ')})',
      );
    }

    final selectedKeys = entity.fields.keys.map(_saveString);
    final whereKeys = fields.map((e) => '${_saveString(e)} = ?');
    final tableName = _saveString(_toPlural('$T'));

    final connection = await Connection.get();
    final results = await connection.query(
      'SELECT ${selectedKeys.join(', ')} FROM $tableName WHERE ${whereKeys.join(' ')}',
      values,
    );

    if (results.isEmpty) {
      return null;
    }

    final data = entity.fields.keys.map((f) => results.first[f]).toList();
    return Function.apply(entity.constructor, data);
  }

  Future<T?> getById(int id) => getBy(['id'], [id]);
}
