import 'package:ironclad/message.dart';

class Ping extends Message with Acknowledge {
  static final MessageArguments<Ping> arguments = {
    (message) => message.timestamp: uint32,
  };
  static Ping constructor(int timestamp) => Ping(timestamp);

  @override
  int get type => 1000;

  @override
  int get timeout => 10000;

  final int timestamp;

  Ping([int? timestamp])
      : timestamp = timestamp ?? DateTime.now().millisecondsSinceEpoch;

  static int calculate(Ping ping) {
    return DateTime.now().millisecondsSinceEpoch - ping.timestamp;
  }
}
