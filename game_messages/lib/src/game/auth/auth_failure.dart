import 'package:ironclad/message.dart';

class AuthFailure extends Message {
  static final MessageArguments<AuthFailure> arguments = {};
  static AuthFailure constructor() => AuthFailure();

  @override
  int get type => 2001;

  AuthFailure();
}
