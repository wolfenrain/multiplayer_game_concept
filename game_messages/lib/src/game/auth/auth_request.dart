import 'package:ironclad/message.dart';

class AuthRequest extends Message {
  static final MessageArguments<AuthRequest> arguments = {
    (message) => message.authToken: string,
  };
  static AuthRequest constructor(String authToken) => AuthRequest(authToken);

  @override
  int get type => 2000;

  final String authToken;

  AuthRequest(this.authToken);
}
