import 'package:ironclad/message.dart';

class AuthSuccess extends Message {
  static final MessageArguments<AuthSuccess> arguments = {};
  static AuthSuccess constructor() => AuthSuccess();

  @override
  int get type => 2002;

  AuthSuccess();
}
