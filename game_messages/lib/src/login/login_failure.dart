import 'package:ironclad/message.dart';

class LoginFailure extends Message {
  static final MessageArguments<LoginFailure> arguments = {};
  static LoginFailure constructor() => LoginFailure();

  @override
  int get type => 101;

  LoginFailure();
}
