import 'package:ironclad/message.dart';

class LoginSuccess extends Message {
  static final MessageArguments<LoginSuccess> arguments = {};
  static LoginSuccess constructor() => LoginSuccess();

  @override
  int get type => 102;

  LoginSuccess();
}
