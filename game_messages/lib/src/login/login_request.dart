import 'package:ironclad/message.dart';

class LoginRequest extends Message with Acknowledge {
  static final MessageArguments<LoginRequest> arguments = {
    (message) => message.username: string,
    (message) => message.password: string,
  };
  static LoginRequest constructor(String username, String password) {
    return LoginRequest(username, password);
  }

  @override
  int get type => 100;

  @override
  int get timeout => 10000;

  final String username;
  final String password;

  LoginRequest(this.username, this.password);
}
