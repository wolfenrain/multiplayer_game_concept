export 'src/login/login_failure.dart';
export 'src/login/login_request.dart';
export 'src/login/login_success.dart';

import 'package:game_messages/src/login/login_failure.dart';
import 'package:game_messages/src/login/login_request.dart';
import 'package:game_messages/src/login/login_success.dart';
import 'package:ironclad/message.dart';

void registerMessages() {
  Message.register<LoginRequest>(
    LoginRequest.arguments,
    LoginRequest.constructor,
  );
  Message.register<LoginFailure>(
    LoginFailure.arguments,
    LoginFailure.constructor,
  );
  Message.register<LoginSuccess>(
    LoginSuccess.arguments,
    LoginSuccess.constructor,
  );
}
