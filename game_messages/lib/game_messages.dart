export 'src/game/auth/auth_failure.dart';
export 'src/game/auth/auth_request.dart';
export 'src/game/auth/auth_success.dart';

export 'src/game/ping/ping.dart';

import 'package:ironclad/message.dart';

import 'src/game/auth/auth_failure.dart';
import 'src/game/auth/auth_request.dart';
import 'src/game/auth/auth_success.dart';
import 'src/game/ping/ping.dart';

void registerMessages() {
  Message.register<AuthRequest>(AuthRequest.arguments, AuthRequest.constructor);
  Message.register<AuthFailure>(
    AuthFailure.arguments,
    AuthFailure.constructor,
  );
  Message.register<AuthSuccess>(
    AuthSuccess.arguments,
    AuthSuccess.constructor,
  );
  Message.register<Ping>(Ping.arguments, Ping.constructor);
}
