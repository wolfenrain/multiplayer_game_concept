import 'package:game_messages/game_messages.dart';
import 'package:game_server/game_server.dart';

void main(List<String> arguments) async {
  registerMessages();

  final server = GameServer(port: 8080);
  server.run();
}
