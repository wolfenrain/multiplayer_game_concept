import 'package:game_messages/game_messages.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/server.dart';

class PingHandler extends MessageHandler<Ping, RemoteClient> {
  Map<RemoteClient, Ping> lastMessage = {};

  @override
  void acknowledge(Ping message, RemoteClient client) {
    // If message is older than last received we can ignore it.
    if (message.id < (lastMessage[client] ?? message).id) {
      return;
    }
    lastMessage[client] = message;
    final ping = Ping.calculate(message);
    client.logger.debug('Current ping: $ping');
    client.logger.debug('Current receving is ${client.receiving} bytes');
    client.logger.debug('Current sending is ${client.sending} bytes');
  }

  @override
  void unacknowledged(Ping message, RemoteClient client) {
    final ping = Ping.calculate(lastMessage[client] ?? message);
    client.logger.debug('Last known ping = $ping');
    if (ping >= 10000) {
      lastMessage.remove(client);
      client.disconnect(reason: 'Ping timeout');
    }
    // if ((lastKeepAliveId[client] ?? 0) < message.id) {
    //   server.onTimeout(client);
    // }
  }
}
