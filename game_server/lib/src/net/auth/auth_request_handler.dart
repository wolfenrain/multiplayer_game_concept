import 'package:game_messages/game_messages.dart';
import 'package:game_server/src/game/components/remote_client_component.dart';
import 'package:game_server/src/game/components/position_component.dart';
import 'package:game_server/src/game/components/slot_component.dart';
import 'package:game_server/src/game/components/velocity_component.dart';
import 'package:game_server/src/game/game_server_ref.dart';
import 'package:game_server/src/game/slot_pool.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/server.dart';
import 'package:vector_math/vector_math_64.dart';

class AuthRequestHandler extends MessageHandler<AuthRequest, RemoteClient>
    with GameServerRef {
  final allowedTokens = ['1234567'];

  @override
  void handle(AuthRequest message, RemoteClient client) {
    if (!allowedTokens.contains(message.authToken)) {
      // TODO: add reason
      return client.send(AuthFailure());
    }

    if (gameServer.slotPool!.noMoreSlotsLeft) {
      // TODO: add reason
      return client.send(AuthFailure());
    }

    // Locking a slot beforehand.
    final slot = gameServer.slotPool!.acquire();

    final entity = world?.createEntity(client.address.host)
      ?..add<RemoteClientComponent, RemoteClient>(client)
      ..add<PositionComponent, Vector2>(Vector2(slot.value * 100, 0))
      ..add<VelocityComponent, Vector2>(Vector2.zero())
      ..add<SlotComponent, Slot>(slot);

    client.onStateChanged<ConnectionState>((state) {
      if (state == ConnectionState.initial) {
        entity?.dispose();
      }
    });

    client.state(ConnectionState.established);
    return client.send(AuthSuccess());
  }
}
