import 'package:ironclad/logger.dart';

const s2micro = 1e6;
const micro2s = 1 / s2micro;
const ms2micro = 1e3;

class GameLoop with Loggable {
  final Stopwatch process = Stopwatch();
  final void Function(double delta) update;

  final int _tickLengthMicro;

  late int _prev;
  late int _target;

  GameLoop(
    this.update, {
    double ticksLength = 1000 / 20,
  }) : _tickLengthMicro = (ticksLength * ms2micro).floor();

  int _elapsedMicros() => process.elapsedMicroseconds;

  void start() {
    process.reset();
    process.start();

    _prev = _elapsedMicros();
    _target = _elapsedMicros();

    _loop();
  }

  void _loop() {
    final now = _elapsedMicros();

    if (now >= _target) {
      final delta = now - _prev;

      _prev = now;
      _target = now + _tickLengthMicro;

      update(delta * micro2s);
    }

    final remainingInTick = _target - _elapsedMicros();
    if (remainingInTick > _tickLengthMicro) {
      logger.error('We shouldnt be getting here!');
    } else {
      Future.delayed(Duration(microseconds: 0), _loop);
    }
  }
}
