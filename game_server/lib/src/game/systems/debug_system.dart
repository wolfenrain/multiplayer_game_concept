import 'package:ironclad/logger.dart';
import 'package:oxygen/oxygen.dart';

class DebugSystem extends System with Loggable {
  int entities = 0;

  @override
  void init() {}

  @override
  void execute(double delta) {
    if (entities != world!.entities.length) {
      entities = world!.entities.length;
      logger.debug('Entities: $entities');
    }
  }
}
