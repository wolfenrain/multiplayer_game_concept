import 'package:game_messages/game_messages.dart';
import 'package:game_server/src/game/components/remote_client_component.dart';
import 'package:oxygen/oxygen.dart';

class PingSystem extends System {
  Query? _query;

  @override
  void init() {
    _query = createQuery([Has<RemoteClientComponent>()]);
  }

  @override
  void execute(double delta) {
    for (final entity in _query?.entities ?? <Entity>[]) {
      final connection = entity.get<RemoteClientComponent>()?.value;
      connection?.send(Ping());
    }
  }
}
