import 'package:game_server/game_server.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/client.dart';
import 'package:oxygen/oxygen.dart';

GameServer? gameServerInstance;

mixin GameServerRef<T extends Message, V extends ClientInterface>
    on MessageHandler<T, V> {
  World? get world => gameServer.world;

  GameServer get gameServer => gameServerInstance!;
}
