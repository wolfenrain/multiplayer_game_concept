import 'package:oxygen/oxygen.dart';

class Slot extends PoolObject<void> {
  static var slotCounter = 0;

  int value = -1;

  @override
  void init([data]) {
    value = value == -1 ? slotCounter++ : value;
  }

  @override
  void reset() {}
}

class SlotPool extends ObjectPool<Slot, void> {
  bool get noMoreSlotsLeft => totalFree == 0;

  SlotPool(int maxSlots) : super(initialSize: maxSlots);

  @override
  Slot builder() => Slot();
}
