import 'package:oxygen/oxygen.dart';
import 'package:vector_math/vector_math_64.dart';

class VelocityComponent extends Component<Vector2> {
  double? x;

  double? y;

  @override
  void init([Vector2? data]) {
    x = data?.x ?? 0;
    y = data?.y ?? 0;
  }

  @override
  void reset() {
    x = null;
    y = null;
  }
}
