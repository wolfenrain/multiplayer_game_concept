import 'dart:io';

import 'package:game_server/src/game/components/remote_client_component.dart';
import 'package:game_server/src/game/components/position_component.dart';
import 'package:game_server/src/game/components/slot_component.dart';
import 'package:game_server/src/game/components/velocity_component.dart';
import 'package:game_server/src/game/slot_pool.dart';
import 'package:game_server/src/game/systems/debug_system.dart';
import 'package:game_server/src/game/systems/ping_system.dart';
import 'package:game_server/src/game/systems/velocity_system.dart';
import 'package:game_server/src/game/game_loop.dart';
import 'package:game_server/src/game/game_server_ref.dart';
import 'package:game_server/src/net/auth/auth_request_handler.dart';
import 'package:game_server/src/net/ping/ping_handler.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/logger.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/server.dart';
import 'package:oxygen/oxygen.dart';
import 'package:vector_math/vector_math_64.dart';

class GameServer with Loggable {
  World? world;
  GameLoop? _gameLoop;
  late final Server _netServer;
  SlotPool? slotPool;

  final int port;

  final int maxSlots;

  GameServer({
    required this.port,
    this.maxSlots = 255,
  }) {
    LogLevel.main = LogLevel.debug;
  }

  void _setupNetServer() {
    logger.debug('Setting up net server for port $port');
    _netServer = Server(
      maxSlots,
      publicAddress: InternetAddress.anyIPv4,
      port: port,
      logLevel: LogLevel.main,
    )
      ..addDirector(
        ConnectionState.anonymous,
        MessageDirector()..attach(AuthRequestHandler()),
      )
      ..addDirector(
        ConnectionState.established,
        MessageDirector()..attach(PingHandler()),
      );

    gameServerInstance = this;
  }

  void _setupWorld() {
    logger.debug('Setting up ECS world');
    world = World();

    // Register components.
    world?.registerComponent<VelocityComponent, Vector2>(
      () => VelocityComponent(),
    );
    world?.registerComponent<PositionComponent, Vector2>(
      () => PositionComponent(),
    );
    world?.registerComponent<RemoteClientComponent, RemoteClient>(
      () => RemoteClientComponent(),
    );
    world?.registerComponent<SlotComponent, Slot>(() => SlotComponent());

    // Register systems.
    world?.registerSystem(DebugSystem());
    world?.registerSystem(PingSystem());
    world?.registerSystem(VelocitySystem());
  }

  void _setupGameLoop() {
    assert(world != null);
    logger.debug('Setting up GameLoop');
    _gameLoop = GameLoop(world!.execute);
  }

  void _setupSlotPool() {
    logger.debug('Setting up slots');
    slotPool = SlotPool(maxSlots);
  }

  void run() {
    _setupNetServer();
    _setupWorld();
    _setupGameLoop();
    _setupSlotPool();

    world?.init();
    _gameLoop?.start();
    _netServer.start();
  }
}
