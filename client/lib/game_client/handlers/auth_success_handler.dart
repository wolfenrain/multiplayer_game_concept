import 'package:game_messages/game_messages.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/message.dart';

class AuthSuccessHandler extends MessageHandler<AuthSuccess, Client> {
  @override
  void handle(AuthSuccess message, Client client) {
    // TODO: onAuthSuccess?
    client.state(ConnectionState.established);
  }
}
