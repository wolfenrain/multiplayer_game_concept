import 'dart:io';

import 'package:client/game_client/handlers/auth_failure_handler.dart';
import 'package:client/game_client/handlers/auth_success_handler.dart';
import 'package:client/game_client/handlers/ping_handler.dart';
import 'package:game_messages/game_messages.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/logger.dart';
import 'package:ironclad/message.dart';

class GameClient with Loggable {
  final InternetAddress _address;

  Client? client;

  GameClient._(this._address) {
    _setupNetworkClient();
  }

  void _setupNetworkClient() {
    logger.debug('Setting up network client for $_address');

    client = Client(
      _address,
      8080,
      version: 'v1',
      logLevel: LogLevel.main,
    )
      ..addDirector(
        ConnectionState.anonymous,
        MessageDirector()
          ..attach(AuthSuccessHandler())
          ..attach(AuthFailureHandler()),
      )
      ..addDirector(
        ConnectionState.established,
        MessageDirector()..attach(PingHandler()),
      );

    client?.onStateChanged<ConnectionState>((state) {
      if (state == ConnectionState.anonymous) {
        client?.send(AuthRequest('1234567'));
      }
    });
  }

  Future<void> connect() async {
    await client?.connect();
  }

  void dispose() {
    client?.dispose();
  }

  static Future<GameClient> create() async {
    final address = (await InternetAddress.lookup('jochum.pc')).first;

    return GameClient._(address);
  }
}
