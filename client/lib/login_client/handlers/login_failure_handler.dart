import 'package:client/login_client/states/authentication_state.dart';
import 'package:game_messages/login_messages.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/message.dart';

class LoginFailureHandler extends MessageHandler<LoginFailure, Client> {
  @override
  void handle(LoginFailure message, Client client) {
    client.state(AuthenticationState.unauthenticated);
  }
}
