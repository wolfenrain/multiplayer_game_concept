import 'package:client/login_client/login_client_ref.dart';
import 'package:client/login_client/states/authentication_state.dart';
import 'package:game_messages/login_messages.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/message.dart';

class LoginSuccessHandler extends MessageHandler<LoginSuccess, Client>
    with LoginClientRef {
  @override
  void handle(LoginSuccess message, Client client) {
    client.state(AuthenticationState.authenticated);
  }
}
