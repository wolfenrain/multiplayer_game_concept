import 'package:game_messages/login_messages.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/message.dart';

class LoginRequestHandler extends MessageHandler<LoginRequest, Client> {
  @override
  void acknowledge(LoginRequest message, Client connection) {}

  @override
  void unacknowledged(LoginRequest message, Client connection) {
    connection.disconnect();
  }
}
