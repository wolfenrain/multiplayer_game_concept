import 'package:ironclad/client.dart';

class AuthenticationState extends ClientState {
  const AuthenticationState(int value) : super(value);

  static const unauthenticated = AuthenticationState(1);
  static const authenticating = AuthenticationState(2);
  static const authenticated = AuthenticationState(3);
}
