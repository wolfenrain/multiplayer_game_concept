import 'package:client/login_client/login_client.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/message.dart';

LoginClient? loginClientInstance;

mixin LoginClientRef<T extends Message, V extends ClientInterface>
    on MessageHandler<T, V> {
  LoginClient get loginClient => loginClientInstance!;
}
