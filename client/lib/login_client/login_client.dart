import 'dart:async';
import 'dart:io';

import 'package:client/game_client/handlers/ping_handler.dart';
import 'package:client/login_client/handlers/login_failure_handler.dart';
import 'package:client/login_client/handlers/login_request_handler.dart';
import 'package:client/login_client/handlers/login_success_handler.dart';
import 'package:client/login_client/states/authentication_state.dart';
import 'package:game_messages/login_messages.dart';
import 'package:ironclad/client.dart';
import 'package:ironclad/logger.dart';
import 'package:ironclad/message.dart';
import 'package:ironclad/util.dart';

class LoginClient with Loggable {
  final InternetAddress _address;

  Client? _client;

  int get receiving => _client?.receiving ?? 0;

  final _connectionCtrl = StreamController<ConnectionState>();
  late Stream<ConnectionState> connectionState;

  final _loginCtrl = StreamController<AuthenticationState>();
  late Stream<AuthenticationState> loginState;

  final _disconnectCtrl = StreamController<DisconnectReason>();
  late Stream<DisconnectReason> onDisconnect;

  LoginClient._(this._address) {
    _setupNetworkClient();
    LogLevel.main = LogLevel.debug;
    onDisconnect = _disconnectCtrl.stream.asBroadcastStream();
    connectionState = _connectionCtrl.stream.asBroadcastStream();
    loginState = _loginCtrl.stream.asBroadcastStream();
  }

  void _setupNetworkClient() {
    _client = Client(
      _address,
      env('LOGIN_SERVER_PORT', 8080),
      version: 'v1',
      logLevel: LogLevel.main,
    )
      ..addDirector(
        ConnectionState.anonymous,
        MessageDirector()
          ..attach(LoginSuccessHandler())
          ..attach(LoginRequestHandler())
          ..attach(LoginFailureHandler()),
      )
      ..addDirector(
        ConnectionState.established,
        MessageDirector()..attach(PingHandler()),
      )
      ..disconnected.listen(_disconnectCtrl.add)
      ..onStateChanged<ConnectionState>((state) {
        if (state == ConnectionState.anonymous) {
          _client?.state(AuthenticationState.unauthenticated);
        }
        _connectionCtrl.add(state);
      })
      ..onStateChanged<AuthenticationState>((state) {
        if (state == AuthenticationState.authenticated) {
          _client?.state(ConnectionState.established);
        }
        if (state == AuthenticationState.unauthenticated) {
          if (_client?.retrieveState<ConnectionState>() ==
              ConnectionState.established) {
            _client?.state(ConnectionState.anonymous);
          }
        }
        _loginCtrl.add(state);
      });
  }

  void login(String username, String password) {
    if (_client?.retrieveState<ConnectionState>() !=
        ConnectionState.anonymous) {
      return logger.warn('Can not login when ConnectionState is not anonymous');
    }
    _client?.state(AuthenticationState.authenticating);
    _client?.send(LoginRequest(username, password));
  }

  Future<void> connect() async {
    if (_client?.states.isEmpty ?? true) {
      _setupNetworkClient();
    }
    await _client?.connect();
  }

  void dispose() => _client?.dispose();

  static Future<LoginClient> create() async {
    return LoginClient._(
        (await InternetAddress.lookup(env('LOGIN_SERVER_HOST', '0.0.0.0')))
            .first);
  }
}
