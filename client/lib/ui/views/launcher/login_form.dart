import 'dart:async';

import 'package:client/login_client/login_client.dart';
import 'package:client/login_client/states/authentication_state.dart';
import 'package:client/ui/form_field.dart';
import 'package:client/ui/framed_container.dart';
import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  final LoginClient loginClient;

  LoginForm(this.loginClient);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  TextEditingController? usernameCtrl;
  TextEditingController? passwordCtrl;

  AuthenticationState? loginState;
  StreamSubscription? streamSubscription;

  @override
  void initState() {
    super.initState();

    streamSubscription = widget.loginClient.loginState.listen(
      onLoginStateChanged,
    );

    usernameCtrl = TextEditingController();
    passwordCtrl = TextEditingController();
  }

  @override
  void dispose() {
    usernameCtrl?.dispose();
    passwordCtrl?.dispose();
    streamSubscription?.cancel();
    super.dispose();
  }

  void onLoginStateChanged(AuthenticationState state) {
    setState(() => loginState = state);
  }

  @override
  Widget build(BuildContext context) {
    return FramedContainer(
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                CustomFormField(
                  label: 'MMO Username',
                  hint: 'Username',
                  controller: usernameCtrl!,
                ),
                SizedBox(height: 16),
                CustomFormField(
                  label: 'MMO Password',
                  hint: 'Password',
                  controller: passwordCtrl!,
                  obscureText: true,
                ),
                SizedBox(height: 16),
                ElevatedButton(
                  onPressed: loginState == AuthenticationState.authenticating
                      ? null
                      : login,
                  child: Text('Login'),
                ),
              ],
            ),
          ),
          if (loginState == AuthenticationState.authenticating)
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              child: Container(
                child: Center(child: CircularProgressIndicator()),
              ),
            ),
        ],
      ),
    );
  }

  void login() {
    widget.loginClient.login(usernameCtrl!.text, passwordCtrl!.text);
  }
}
