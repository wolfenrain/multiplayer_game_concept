import 'dart:async';

import 'package:client/login_client/login_client.dart';
import 'package:client/ui/framed_container.dart';
import 'package:client/ui/views/launcher/login_form.dart';
import 'package:flutter/material.dart' hide ConnectionState;
import 'package:ironclad/client.dart';

class GameLauncher extends StatefulWidget {
  final LoginClient loginClient;

  const GameLauncher(this.loginClient);

  @override
  _GameLauncherState createState() => _GameLauncherState();
}

class _GameLauncherState extends State<GameLauncher> {
  ConnectionState? connectionState;
  StreamSubscription? streamSubscription;

  @override
  void initState() {
    super.initState();

    streamSubscription = widget.loginClient.connectionState.listen((state) {
      setState(() => connectionState = state);
    });

    widget.loginClient.onDisconnect.listen((event) async {
      if (connectionState == ConnectionState.initial) {
        return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Connection failed'),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Failed to connection to the server'),
                  SizedBox(height: 8),
                  Text('Error code: ${event.errorCode}'),
                  Text('Reason: ${event.reason}'),
                ],
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    widget.loginClient.connect();
                    Navigator.of(context).pop();
                  },
                  child: Text('Try again'),
                ),
              ],
            );
          },
        );
      }
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Disconnected'),
            content: Text('Lost connection to the server'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  widget.loginClient.connect();
                  Navigator.of(context).pop();
                },
                child: Text('Reconnect'),
              ),
            ],
          );
        },
      );
    });

    widget.loginClient.connect();
  }

  @override
  void dispose() {
    streamSubscription?.cancel();
    super.dispose();
  }

  Widget buildStartGame() {
    return FramedContainer(
      height: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          ElevatedButton(
            onPressed: () {},
            child: Text('Start game'),
          ),
        ],
      ),
    );
  }

  Widget buildOverview() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 920),
        child: Row(
          children: [
            Expanded(
              child: FramedContainer(
                height: 200,
                child: Text(
                  'hi',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(width: 16),
            if (connectionState == ConnectionState.established)
              Expanded(child: buildStartGame())
            else
              Expanded(child: LoginForm(widget.loginClient)),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Image.network(
              'https://wallpaperaccess.com/full/380883.jpg',
              fit: BoxFit.cover,
            ),
          ),
          Center(child: buildOverview()),
          if (connectionState == ConnectionState.initial)
            Container(
              color: Colors.grey.withOpacity(0.5),
              child: Center(child: CircularProgressIndicator()),
            ),
        ],
      ),
    );
  }
}
