import 'package:flutter/material.dart';

class FramedContainer extends StatelessWidget {
  final Widget child;

  final sliceSize = 8.0;

  final double? width;

  final double? height;

  const FramedContainer({
    Key? key,
    this.width,
    this.height,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Color(0xFF090f1f).withOpacity(0.75),
        image: DecorationImage(
          image: AssetImage('window.png'),
          centerSlice: Rect.fromLTWH(
            sliceSize,
            sliceSize,
            sliceSize,
            sliceSize,
          ),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.all(sliceSize),
        child: child,
      ),
    );
  }
}
