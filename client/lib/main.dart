import 'package:client/login_client/login_client.dart';
import 'package:client/login_client/login_client_ref.dart';
import 'package:client/ui/views/launcher/game_launcher.dart';
import 'package:flutter/material.dart' hide ConnectionState;
import 'package:game_messages/game_messages.dart' as game;
import 'package:game_messages/login_messages.dart' as login;
import 'package:ironclad/logger.dart';

void main() async {
  game.registerMessages();
  login.registerMessages();

  LogLevel.main = LogLevel.info;

  final loginClient = loginClientInstance = await LoginClient.create();

  runApp(MyApp(loginClient));
}

class MyApp extends StatefulWidget {
  final LoginClient loginClient;

  MyApp(this.loginClient);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: GameLauncher(widget.loginClient),
    );
  }
}
